# frozen_string_literal: true

commands/ping.rb'

RSpec.describe Ping do
  describe '#main!' do
    context 'when the command is triggered' do
      let(:event) { double }
      let(:response) { 'pong' }

      it 'sends a message to the chat' do
        expect(event).to receive(:send_message)
          .with(response)

        described_class.new.main!(event)
      end
    end
  end

  describe '#description' do
    let(:response) { "This is a test command to see if I'm still alive" }

    it { expect(described_class.new.description).to eq(response) }
  end
end
