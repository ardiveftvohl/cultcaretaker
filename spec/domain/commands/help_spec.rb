# frozen_string_literal: true

require './app/domain/commands/help.rb'

RSpec.describe Help do
  describe '#main!' do
    context 'when the command is triggered' do
      let(:event) { double }

      before do
        stub_const(
          'Ping', double(new: double(description: 'test description'))
        )
      end

      it 'sends an embed to the chat' do
        expect(event).to receive(:send_embed)

        described_class.new.main!(event)
      end
    end
  end

  describe '#description' do
    let(:response) do
      'This will show the help menu, ' \
      'it lists all the possible commands you can give'
    end

    it { expect(described_class.new.description).to eq(response) }
  end
end
