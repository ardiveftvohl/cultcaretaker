# frozen_string_literal: true

require 'discordrb'
require 'simplecov'

ENV['BOT_ENV'] = 'testing'

RSpec.configure do |config|
  SimpleCov.start

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end

def message_event(content:, author_id:, server_id:)
  double('event',
         content: content,
         channel: double('channel', "private?": false),
         server: double('server', id: server_id, resolve_id: server_id),
         author: double('author', id: author_id, resolve_id: author_id),
         timestamp: double('timestamp'))
end

def dispatch(klass, event, bot)
  allow(event).to receive(:"is_a?").with(klass).and_return(true)
  bot.event_handlers[klass].each do |handler|
    handler.match(event)
  end
end

module Discordrb
  module EventContainer
    attr_reader :event_handlers
  end
end
