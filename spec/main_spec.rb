# frozen_string_literal: true

require './app/main'
require './app/domain/config'

RSpec.describe Main do
  describe '#main' do
    let(:token) { 'RandomToken' }
    let(:bot) { Discordrb::Bot.new(token: token) }
    let(:event) do
      message_event(
        content: message_content,
        author_id: 1,
        server_id: 2
      )
    end

    subject do
      described_class.new(token: token).main!
    end

    context 'when the right prefix is provided' do
      context 'when an existing command is called' do
        let(:message_content) { "#{Config.load('prefix')}ping" }

        it 'executes the command' do
          expect(Discordrb::Bot).to receive(:new)
            .with(token: token)
            .and_return bot

          subject

          expect_any_instance_of(Ping).to receive(:main!)
          dispatch(Discordrb::Events::MessageEvent, event, bot)
        end
      end

      context 'when a nonexiting command is called' do
        let(:message_content) { "#{Config.load('prefix')}this_command_does_not_exist" }
        let(:error_message) { 'This command does not exist' }
        let(:error_time) { 3 }

        before do
          stub_const('NO_COMMAND_ERROR', error_message)
        end

        it 'displays an error message to the discord server' do
          expect(Discordrb::Bot).to receive(:new)
            .with(token: token)
            .and_return bot

          subject

          expect(event).to receive(:send_temporary_message)
            .with(error_message, error_time)
          dispatch(Discordrb::Events::MessageEvent, event, bot)
        end
      end
    end

    context 'when the wrong prefix is provided' do
      let(:message_content) { 'ping' }

      it 'ignores the message' do
        expect(Discordrb::Bot).to receive(:new)
          .with(token: token)
          .and_return bot

        subject

        dispatch(Discordrb::Events::MessageEvent, event, bot)
      end
    end
  end
end
