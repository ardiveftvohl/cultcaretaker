# frozen_string_literal: true

require_relative 'realmeye/api/user'
require_relative 'realmeye/api/graveyard'

class VetVerification
  class << self
    def minimum?(name)
      @user_data = Realmeye::Api::User.new(name)
      @graveyard_data = Realmeye::Api::Graveyard.new(name)

      return unless private_check
      return unless run_requirement_check

      true
    end

    def private_check
      return true if @user_data.hidden_location?
    end

    def run_requirement_check 
      return true if @graveyard_data.cult_veteran?
    end
  end
end
