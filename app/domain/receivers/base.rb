# frozen_string_literal: true

require 'discordrb'
require_relative 'bot_receivers/base_receiver.rb'

module Receivers
  class Base
    def start_receivers(bot, receivers = load_receivers)
      receivers.each do |receiver|
        Object.const_get('Receivers::BotReceivers::' + receiver).new.main!(bot)
      end
    end

    private

    def load_receivers
      files = Dir["#{File.dirname(__FILE__)}/bot_receivers/**/*.rb"].map do |file|
        require File.absolute_path(file)
        File.basename(file, '.*')
      end

      files.map do |filename|
        filename.split('_').map(&:capitalize).join
      end
    end
  end
end
