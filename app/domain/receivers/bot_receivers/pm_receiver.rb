# frozen_string_literal: true

require 'discordrb'
require_relative '../../actions/pm/base.rb'

module Receivers
  module BotReceivers
    class PmReceiver < BaseReceiver
      def main!(bot)
        bot.pm do |event|
          Actions::Pm::Base.new.main!(event)
        end
      end
    end
  end
end
