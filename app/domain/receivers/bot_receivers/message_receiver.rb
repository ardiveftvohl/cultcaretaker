# frozen_string_literal: true

require 'discordrb'

module Receivers
  module BotReceivers
    class MessageReceiver < BaseReceiver
      def main!(bot)
        command_executor = Commands::Base.new

        bot.message do |event|
          command_executor.main!(event)
        end
      end
    end
  end
end
