# frozen_string_literal: true

require 'discordrb'
require_relative '../../actions/reaction/base.rb'

module Receivers
  module BotReceivers
    class ReactionReceiver < BaseReceiver
      def main!(bot)
        bot.reaction_add do |event|
          Actions::Reaction::Base.new.main!(event)
        end
      end
    end
  end
end
