# frozen_string_literal: true

require 'discordrb'

module Receivers
  module BotReceivers
    class BaseReceiver
      def main!(bot); end
    end
  end
end
