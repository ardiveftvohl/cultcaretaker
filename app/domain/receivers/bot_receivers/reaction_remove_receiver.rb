# frozen_string_literal: true

require 'discordrb'
require_relative '../../actions/reaction_remove/base.rb'

module Receivers
  module BotReceivers
    class ReactionRemoveReceiver < BaseReceiver
      def main!(bot)
        bot.reaction_remove do |event|
          Actions::ReactionRemove::Base.new.main!(event)
        end
      end
    end
  end
end
