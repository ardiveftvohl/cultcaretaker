# frozen_string_literal: true

require 'discordrb'
require_relative 'command.rb'

module Commands
  class CommandList
    class CommandInfo
      attr_reader :name, :description
      def initialize(name, description)
        @name = name.downcase
        @description = description
      end
    end

    def initialize
      @@commands ||= load_commands
    end

    def command(name)
      return Object.const_get('Commands::BotCommands::' + name).new if @@commands.include? name
    end

    def all
      commands = []
      @@commands.each do |command|
        commands << CommandInfo.new(command, command_description(command))
      end
      commands
    end

    def command_description(name)
      command(name).description
    end

    private

    def load_commands
      @@commands = Dir["#{File.dirname(__FILE__)}/bot_commands/**/*.rb"].map do |file|
        require File.absolute_path(file)
        File.basename(file, '.*').capitalize
      end
    end
  end
end
