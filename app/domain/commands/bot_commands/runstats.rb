# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Runstats < Command
      def main!(event)
        return insufficient_perms(event) unless Helpers::User.staff?(event.author)
        return send_no_user_specified_error(event.channel) if user(event.message).nil?
        return send_no_user_specified_error(event.channel) unless parse_command(event.message.content)

        Senders::Message.new.embed(embed, event.channel)
        event.message.delete
      end

      def description
        ";runstats [user]\n" \
        'This command shows how many runs someone has ran'
      end

      private

      def insufficient_perms(event)
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
        event.message.delete
      end

      def parse_command(message)
        args = message.split(' ')
        if args.length == 1
          weekly_quota_check
        elsif args.length == 2
          all_time_runs
        elsif args.length != 4
          return false
        else
          invalid = args[2].to_i.zero? || !%w[d w m].include?(args[3].downcase)
          return false if invalid

          timed_runs(args[2].to_i, args[3].downcase)
        end
        true
      end

      def timed_runs(amount, timeframe)
        from = Time.now - (amount * to_seconds(timeframe))
        @runs = Storage::Logs.run.fetch(user.id, from)
        @from = "in the past #{amount} #{letter_to_word(timeframe)}#{amount == 1 ? '' : 's'}"
      end

      def letter_to_word(timeframe)
        case timeframe
        when 'd'
          'day'
        when 'w'
          'week'
        when 'm'
          'month'
        end
      end

      def all_time_runs
        @runs = Storage::Logs.run.fetch(user.id, nil)
        @from = ''
      end

      def weekly_quota_check
        from = Time.now - to_seconds('w')
        @runs = Storage::Logs.run.fetch(nil, from)
        @runs = 'in the past 1 week'
      end

      def to_seconds(timeframe)
        case timeframe
        when 'd'
          3600 * 24
        when 'w'
          3600 * 24 * 7
        when 'm'
          3600 * 24 * 31
        end
      end

      def send_no_user_specified_error(channel)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), channel)
      end

      def send_no_user_data_error(channel)
        Senders::Message.new.temp_message(Config.load('no_user_data_error'), channel)
      end

      def embed
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "Run stats for #{user.nick} #{@from}",
          icon_url: user.avatar_url
        )
        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          fields: fields,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def fields
        [
          Discordrb::Webhooks::EmbedField.new(
            'name': 'Successful ✅',
            'value': extract_runs(@runs[true]),
            'inline': true
          ),
          Discordrb::Webhooks::EmbedField.new(
            'name': 'Failed ❌',
            'value': extract_runs(@runs[false]),
            'inline': true
          )
        ]
      end

      def extract_runs(record)
        "Lead: #{record['leader']}\nParsed: #{record['parser']}\nAssist: #{record['assist']}"
      end

      def user(message = nil)
        return @user if defined? @user
        return nil if message.mentions.empty?

        @user ||= message.mentions&.first.on(Config.load('server_id'))
      end
    end
  end
end
