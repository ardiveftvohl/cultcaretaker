# frozen_string_literal: true

require 'time'
require 'discordrb'
require './app/domain/messages/dashboard.rb'

module Commands
  module BotCommands
    class Afk < Command
      def main!(event)
        return insufficient_perms(event) unless raid_leader(event.user)
        return wrong_channel(event.channel) unless event.channel == Discord::Channel.raid_command(event.server).get
        return invalid_command(event.channel) unless command_check(event.message.content)
        return afk_already_active(event.channel) if other_afk_message(event.server)

        setup_channel(event)
        setup_dashboard(event)
        setup_afk(event)
        event.message.delete
      end

      def description
        ";afk [channel number 1-5] [server] [location]\n" \
        'This command will start an afk check and opens a raiding channel for everyone'
      end

      def setup_channel(event)
        @channel = Discord::Channel.raid_voice(event.server, channel_number(event.message.content))
        Storage::Afk.new(@channel.id, server(event.message.content), location(event.message.content), event.user)
        @channel.open(Config.load('roles')['verified_raider_role'])
      end

      def setup_dashboard(event)
        Messages::Dashboard.new(
          event.server, event.bot, event.user, server(event.message.content),
          location(event.message.content), @channel.name, channel_number(event.message.content)
        )
        Messages::Dashboard.send_message(event.channel)
      end

      def setup_afk(event)
        message = send_afk(event)
        add_reactions(message, %w[
          cultist lost_halls_key warrior paladin knight
          mystic trickster puri mseal slow qot brain nitro
        ])
      end

      private

      def emoji(name, server)
        Helpers::Emoji.get(name, server)
      end

      def send_afk(event)
        message = Senders::Message.new.embed(afk_embed(event), Discord::Channel.raid_announcement(event.server).get, '@here')
        message.edit('', afk_embed(event))
        message
      end

      def afk_embed(event)
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: format(Config.load('afk_check_title'), event.user.nick, @channel.name),
          icon_url: event.user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: afk_embed_description(event.server),
          color: '#00ff0d',
          author: author,
          timestamp: Time.now
        )

        embed
      end

      def afk_embed_description(server)
        cultist_emoji = emoji('cultist', server)
        key_emoji = emoji('lost_halls_key', server)
        warrior_emoji = emoji('warrior', server)
        paladin_emoji = emoji('paladin', server)
        knight_emoji = emoji('knight', server)
        mystic_emoji = emoji('mystic', server)
        trickster_emoji = emoji('trickster', server)
        puri_emoji = emoji('puri', server)
        mseal_emoji = emoji('mseal', server)
        slow_emoji = emoji('slow', server)
        qot_emoji = emoji('qot', server)
        brain_emoji = emoji('brain', server)
        nitro_emoji = emoji('nitro', server)

        format(
          Config.load('afk_check_message'),
          cultist_emoji, key_emoji, warrior_emoji, paladin_emoji,
          knight_emoji, mystic_emoji, trickster_emoji, puri_emoji,
          mseal_emoji, slow_emoji, qot_emoji, brain_emoji, nitro_emoji
        )
      end

      def add_reactions(message, emojis)
        Senders::Reaction.add_reactions(message, emojis)
      end

      def command_check(message)
        message.split(' ').count == 4
      end

      def raid_leader(user)
        return Helpers::User.staff?(user) unless Helpers::User.security?(user)

        Helpers::User.head_raid_leader?(user) || Helpers::User.veteran_raid_leader?(user) || Helpers::User.raid_leader?(user) || Helpers::User.almost_raid_leader?(user)
      end

      def insufficient_perms(event)
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
        event.message.delete
      end

      def other_afk_message(server)
        true if Discord::Channel.raid_announcement(server).find_bot_message('AFK Check Started')
      end

      def invalid_command(channel)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), channel)
      end

      def wrong_channel(channel)
        Senders::Message.new.temp_message(Config.load('wrong_channel_error'), channel)
      end

      def afk_already_active(channel)
        Senders::Message.new.message(Config.load('afk_check_running_error'), channel)
      end

      def channel_number(message)
        message.split(' ')[1].to_i
      end

      def server(message)
        message.split(' ')[2].upcase
      end

      def location(message)
        message.split(' ')[3].upcase
      end
    end
  end
end
