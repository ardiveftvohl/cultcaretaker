# frozen_string_literal: true

require 'discordrb'
require 'time'
require './app/domain/parsers/user_parser.rb'
require './app/domain/messages/dashboard.rb'

module Commands
  module BotCommands
    class Parse < Command
      def main!(event)
        return invalid_command(event) if channel_number(event.content).zero? || channel_number > 3

        @event = event
        update_dashboard(event.user.id)
        event.channel.start_typing
        send_embed(embed(fields), event.channel)
        event.message.delete
      end

      def description
        ";parse [channel number]\n" \
        'This command parses all the users in a channel'
      end

      private

      def update_dashboard(user_id)
        return unless Messages::Dashboard.afk_ended?

        Messages::Dashboard.parser = user_id
        Messages::Dashboard.update
      end

      def parsers
        @parsers ||= Discord::Channel.raid_voice(@event.server, channel_number).users.collect do |u|
          Parsers::UserParser.new(u)
        end
      end

      def fields
        fields = []
        parsers.each do |p|
          descr = ''
          next descr += "#{invisible(p)}\n" unless p.character?
          descr += "#{wrong_stats(p)}\n" unless p.parse_attack.zero? && p.parse_dexterity.zero?
          descr += "#{wrong_equips(p)}\n" unless correct_equips(p).empty?
          fields << make_field("#{p.name}'s #{emoji(p.class.downcase)}", descr) unless descr.empty?
        end
        if fields.empty?
          fields << make_field('Everyone is clear', 'Checked for attack, dexterity, and equipment')
        end
        fields
      end

      def make_field(name, value, inline = false)
        Discordrb::Webhooks::EmbedField.new('name': name, 'value': value, 'inline': inline)
      end

      def invisible(p)
        'Has an invisible realmeye or does not exist'
      end

      def wrong_stats(p)
        out = 'Is missing'
        out += " #{p.parse_attack}#{emoji('att')}" unless p.parse_attack.zero?
        out += " #{p.parse_dexterity}#{emoji('dex')}" unless p.parse_dexterity.zero?
        out
      end

      def wrong_equips(p)
        "Does not meet #{correct_equips(p)} reqs"
      end

      def correct_equips(parser)
        missing = []
        %w[weapon ability armor].each do |slot|
          missing << slot unless parser.parse_slot?(slot)
        end
        missing.join(', ')
      end

      def embed(fields)
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "Parsing Results for Channel #{channel_number}",
          icon_url: @event.user.avatar_url
        )
        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          fields: fields,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def send_embed(message, channel)
        Senders::Message.new.embed(message, channel)
      end

      def invalid_command(event)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), event.channel)
        event.message.delete
      end

      def emoji(name)
        Helpers::Emoji.get(name, @event.server)
      end

      def channel_number(message = nil)
        @channel_number ||= message.split(' ')[1].to_i
      end
    end
  end
end
