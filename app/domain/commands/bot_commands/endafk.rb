# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Endafk < Command
      def main!(event, user = nil)
        return insufficient_perms(event) unless admin(event.user)
        return wrong_channel(event.channel) unless event.channel == Discord::Channel.raid_command(event.server).get
        @user = user.nil? ? event.user : user

        event.message.delete
        lock_raiding_channel(event.server)

        users = last_afk_message(event.server).reacted_with(Helpers::Emoji.get('cultist', event.server), limit: 100)
        raiding_channel.evict_others(users, Discord::Channel.lounge(event.server).get)
        message = last_afk_message(event.server).edit('', post_afk_embed(raiding_channel.name, 30))

        countdown(message)
      end

      def description
        'This command starts a 30 seconds post-afk'
      end

      private

      def countdown(message)
        [25, 20, 15, 10, 5].each do |time_left|
          sleep(5)
          message.edit('', post_afk_embed(raiding_channel.name, time_left))
        end
        sleep(5)
        message.edit('', end_afk_embed(raiding_channel.name))
      end

      def post_afk_embed(channel_name, time_left)
        description = format(Config.load('afk_end_message'), time_left.to_s, channel_name)
        footer = Discordrb::Webhooks::EmbedFooter.new(
          text: "Ended by #{@user.nick}",
          icon_url: @user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: description,
          color: '#ffbb00',
          timestamp: Time.now,
          footer: footer
        )

        embed
      end

      def end_afk_embed(channel_name)
        description = format(Config.load('afk_closed_message'), channel_name)
        footer = Discordrb::Webhooks::EmbedFooter.new(
          text: "Ended by #{@user.nick}",
          icon_url: @user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: description,
          color: 'ff0000',
          timestamp: Time.now,
          footer: footer
        )

        embed
      end

      def lock_raiding_channel(server)
        raiding_channel(server).lock(Config.load('roles')['verified_raider_role'])
      end

      def raiding_channel(server = nil)
        @raiding_channel ||= Discord::Channel.raid_voice(server, channel_number(server))
      end

      def channel_number(server)
        last_afk_message(server).embeds.first.author.name.match(/Raiding (\d)/)[1]
      end

      def last_afk_message(server)
        Discord::Channel.raid_announcement(server).find_bot_message('AFK Check Started')
      end

      def invalid_command(channel)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), channel)
      end

      def wrong_channel(channel)
        Senders::Message.new.temp_message(Config.load('wrong_channel_error'), channel)
      end

      def admin(user)
        Helpers::User.owner?(user) || Helpers::User.admin?(user) || Helpers::User.head_developer?(user) || Helpers::User.moderator?(user) || Helpers::User.bot?(user)
      end

      def insufficient_perms(event)
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
        event.message.delete
      end
    end
  end
end
