# frozen_string_literal: true

require 'discordrb'

module Commands
  module BotCommands
    class Vetafk < Command
      def main!(event)
        return wrong_channel(event.channel) unless event.channel == Discord::Channel.vet_raid_command(event.server).get
        return invalid_command(event.channel) unless command_check(event.message.content)
        return afk_already_active(event.channel) if other_afk_message(event.server)

        @channel = Discord::Channel.vet_raid_voice(event.server, channel_number(event.message.content))
        message = send_afk(event)

        Storage::Vetafk.new(@channel.id, server(event.message.content), location(event.message.content))
        @channel.open(Config.load('roles')['veteran_raider_role'])
        add_reactions(message)
      end

      def description
        ";vetafk [channel number 1-5] [server] [location]\n" \
        'This command will start an afk check for a **Veteran** raid and opens a raiding channel for everyone'
      end

      private

      def emoji(name, server)
        Helpers::Emoji.get(name, server)
      end

      def send_afk(event)
        message = Senders::Message.new.embed(embed(event), Discord::Channel.vet_raid_announcement(event.server).get, '@here')
        message.edit('', embed(event))
        message
      end

      def embed(event)
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: format(Config.load('vet_afk_check_title'), event.user.nick, @channel.name),
          icon_url: event.user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: embed_description(event.server),
          color: '#00ff0d',
          author: author,
          timestamp: Time.now
        )

        embed
      end

      def embed_description(server)
        cultist_emoji = emoji('cultist', server)
        key_emoji = emoji('lost_halls_key', server)
        warrior_emoji = emoji('warrior', server)
        paladin_emoji = emoji('paladin', server)
        knight_emoji = emoji('knight', server)
        mystic_emoji = emoji('mystic', server)
        trickster_emoji = emoji('trickster', server)
        puri_emoji = emoji('puri', server)
        mseal_emoji = emoji('mseal', server)
        slow_emoji = emoji('slow', server)
        qot_emoji = emoji('qot', server)
        brain_emoji = emoji('brain', server)
        nitro_emoji = emoji('nitro', server)
        x_emoji = emoji('x', server)

        format(
          Config.load('vet_afk_check_message'),
          cultist_emoji, key_emoji, warrior_emoji, paladin_emoji,
          knight_emoji, mystic_emoji, trickster_emoji, puri_emoji,
          mseal_emoji, slow_emoji, qot_emoji, brain_emoji,
          nitro_emoji, x_emoji
        )
      end

      def add_reactions(message)
        emojis = %w[
          cultist lost_halls_key warrior paladin knight
          mystic trickster puri mseal slow qot brain nitro x
        ]
        Senders::Reaction.add_reactions(message, emojis)
      end

      def command_check(message)
        message.split(' ').count == 4
      end

      def other_afk_message(server)
        true if Discord::Channel.vet_raid_announcement(server).find_bot_message('AFK Check Started')
      end

      def invalid_command(channel)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), channel)
      end

      def wrong_channel(channel)
        Senders::Message.new.temp_message(Config.load('wrong_channel_error'), channel)
      end

      def afk_already_active(channel)
        Senders::Message.new.message(Config.load('afk_check_running_error'), channel)
      end

      def channel_number(message)
        message.split(' ')[1].to_i
      end

      def server(message)
        message.split(' ')[2].upcase
      end

      def location(message)
        message.split(' ')[3].upcase
      end
    end
  end
end
