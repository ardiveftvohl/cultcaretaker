# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Masterverify < Command
      def main!(event)
        return insufficient_perms(event) unless upper_staff(event.author)

        Senders::Message.new.embed(verification(event), event.channel)
        event.message.delete
      end

      private

      def verification(event)
        desc = "The following users have now been verified:\n\n#{verify_nicknamed(event.server)}"
        desc = "All nicknamed users currently have the <@&#{verified_raider}> role." unless desc.include?('@')

        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "#{event.user.nick} has initiated a mass verification",
          icon_url: event.user.avatar_url
        )
        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          description: desc,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def verify_nicknamed(server)
        verified = []
        server.members.each do |member|
          next if member.nick.nil? || member.role?(verified_raider) || member.role?(staff)

          Helpers::User.assign_role(member, verified_raider)
          verified << "<@!#{member.id}>"
        end
        verified.join(' ')
      end

      def verified_raider
        Config.load('roles')['verified_raider_role']
      end

      def bot_role
        Config.load('roles')['staff']['bot']
      end

      def insufficient_perms(event)
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
        event.message.delete
      end

      def upper_staff(user)
        Helpers::User.owner?(user) || Helpers::User.head_developer?(user)
      end
    end
  end
end
