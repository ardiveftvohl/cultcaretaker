# frozen_string_literal: true

require 'discordrb'

module Commands
  module BotCommands
    class Ping < Command
      def main!(event)
        Senders::Message.new.message('pong', event.channel)
      end

      def description
        "This is a test command to see if I'm still alive"
      end
    end
  end
end
