# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Help < Command
      def main!(event)
        Senders::Message.new.embed(embed, event.channel)
      end

      def description
        'This will show the help menu, it lists all the possible commands ' \
        'you can give'
      end

      private

      def embed
        embed = Discordrb::Webhooks::Embed.new(
          title: 'Here are the possible commands you can give me',
          description: embed_description,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def embed_description
        embed_description = ''
        CommandList.new.all.each do |command|
          embed_description += "**#{Config.load('prefix')}#{command.name}**```#{command.description}```\n"
        end
        embed_description
      end
    end
  end
end
