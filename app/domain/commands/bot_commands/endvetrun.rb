# frozen_string_literal: true

require 'discordrb'

module Commands
  module BotCommands
    class Endvetrun < Command
      def main!(event)
        return unless Helpers::User.staff?(event.author)
        return send_invalid_commend_error(event.channel) unless valid_command?(event)
        return send_invalid_channel_error(event.channel) unless edit_afk_message(event)

        Storage::Logs.run.add(raidleader, assists(event.message), run_result(event.content))

        Senders::Message.new.temp_message(Config.load('vet_run_ended_relay_message'), event.channel)
      end

      def description
        ";endvetrun [channel number 1-5] [s or f(success or failure)] [leader] [assist]\n" \
        'This command is used to end the run once post afk has finished'
      end

      private

      def valid_command?(event)
        return false if raidleader(event.message).nil?
        return false if run_result(event.content).nil?
        return false unless channel_number(event.content)
        return false unless valid_message_length?(event.content)

        true
      end

      def edit_afk_message(event)
        afk_closed_message = format(Config.load('vet_afk_closed_message'), channel(event).name)
        message = Discord::Channel.vet_raid_announcement(event.server).find_bot_message(afk_closed_message)

        return if message.nil?

        message.edit('', embed(event))
      end

      def send_invalid_commend_error(channel)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), channel)
      end

      def send_invalid_channel_error(channel)
        Senders::Message.new.temp_message(Config.load('no_run_in_channel_error'), channel)
      end

      def valid_message_length?(message)
        message.split.count >= 4
      end

      def embed(event)
        description = format(Config.load('vet_run_ended_message'), channel(event).name)
        footer = Discordrb::Webhooks::EmbedFooter.new(
          text: "Ended by #{event.user.nick}",
          icon_url: event.user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: description,
          timestamp: Time.now,
          footer: footer
        )

        embed
      end

      def channel(event)
        Discord::Channel.vet_raid_voice(event.server, channel_number(event.content))
      end

      def channel_number(message)
        @channel_number ||= message.split[1]

        begin
          Integer(@channel_number)
        rescue StandardError
          false
        end
      end

      def run_result(message)
        case message.split[2]
        when 's'
          true
        when 'f'
          false
        end
      end

      def raidleader(message = nil)
        @raidleader ||= message.mentions.first&.id
      end

      def assists(message)
        message.mentions[1...]&.map(&:id)
      end
    end
  end
end
