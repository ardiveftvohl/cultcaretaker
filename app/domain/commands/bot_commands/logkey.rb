# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Logkey < Command
      def main!(event, logger_overwrite = nil)
        return insufficient_perms(event) unless Helpers::User.staff?(event.author)
        return send_wrong_channel_error(event) unless raiding_channel(event)
        return send_no_user_data_error(event) if user(event.message).nil?

        @logger_overwrite = logger_overwrite
        amount = Storage::Logs.key.add(user.id, key_amount(event.content))
        Senders::Message.new.embed(embed(amount, event.user, nickname(event.content)), event.channel)
        event.message.delete
      end

      def description
        ";logkey [user] (#of keys)\n" \
        'This command is used to log the key when someone pops a key'
      end

      private

      def raiding_channel(event)
        normal = event.channel == Discord::Channel.raid_announcement(event.server).get
        veteran = event.channel == Discord::Channel.vet_raid_announcement(event.server).get
        normal || veteran
      end

      def embed(amount, user, nickname)
        return no_keys_popped(user, nickname) if amount.zero?

        desc = "#{nickname} has now popped a total of **#{amount} #{(amount > 1 ? 'keys' : 'key')} ** for this discord"
        user = @logger_overwrite unless @logger_overwrite.nil?
        footer = Discordrb::Webhooks::EmbedFooter.new(
          text: "#{delta(key_amount)} logged by #{user.nick}",
          icon_url: user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: desc,
          timestamp: Time.now,
          footer: footer
        )

        embed
      end

      def no_keys_popped(user, nickname)
        desc = "#{nickname} hasn't popped any keys for this discord"
        user = @logger_overwrite unless @logger_overwrite.nil?
        footer = Discordrb::Webhooks::EmbedFooter.new(
          text: "#{delta(key_amount)} logged by #{user.nick}",
          icon_url: user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: desc,
          timestamp: Time.now,
          footer: footer
        )

        embed
      end

      def delta(key_amount)
        return '1 key' if key_amount.nil?

        "#{key_amount} keys"
      end

      def send_no_user_data_error(event)
        Senders::Message.new.temp_message(Config.load('no_user_data_error'), event.channel)
        event.message.delete
      end

      def send_wrong_channel_error(event)
        Senders::Message.new.temp_message(Config.load('wrong_channel_error'), event.channel)
        event.message.delete
      end

      def insufficient_perms(event)
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
        event.message.delete
      end

      def user(message = nil)
        @user ||= message.mentions&.first
      end

      def nickname(message = nil)
        @nickname ||= message.split[1]
      end

      def key_amount(message = nil)
        return @key_amount if defined? @key_amount

        @key_amount = message.split[2]&.to_i
      end
    end
  end
end
