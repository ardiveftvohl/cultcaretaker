# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Poll < Command
      def main!(event)
        poll_type = event.message.content.split(' ')[1]
        event.message.delete
        return unless raiding_channel(event)

        if poll_type.downcase == 'server'
          message = Senders::Message.new.embed(server_poll_embed, event.channel)
          emojis = %w[eu us]
          add_reactions(message, emojis)
        elsif poll_type.downcase == 'sentry'
          message = Senders::Message.new.embed(sentry_poll_embed(event.server), event.channel)
          emojis = %w[sentry lost_halls_key]
          add_reactions(message, emojis)
        end
      end

      def description
        ";poll sentry\n" \
        ";poll server\n" \
        "This command starts a server or sentry poll\n" \
        'This command must be used in a raid anouncement channel'
      end

      private

      def raiding_channel(event)
        normal = event.channel == Discord::Channel.raid_announcement(event.server).get
        veteran = event.channel == Discord::Channel.vet_raid_announcement(event.server).get
        normal || veteran
      end

      def server_poll_embed
        Discordrb::Webhooks::Embed.new(
          title: 'On which servers should we do the next few runs?',
          description: 'EU 🇪🇺 or US 🇺🇸',
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )
      end

      def sentry_poll_embed(server)
        sentry_emoji = Helpers::Emoji.get('sentry', server)
        key_emoji = Helpers::Emoji.get('lost_halls_key', server)
        Discordrb::Webhooks::Embed.new(
          title: 'Should we do a sentry run, or wait for a key?',
          description: format('Sentry %s or Key %s', sentry_emoji, key_emoji),
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )
      end

      def add_reactions(message, emojis)
        Senders::Reaction.add_reactions(message, emojis)
      end
    end
  end
end
