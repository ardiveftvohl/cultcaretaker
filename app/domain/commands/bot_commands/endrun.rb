# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Endrun < Command
      def main!(event, user = nil)
        @user = user.nil? ? event.user : user
        return insufficient_perms(event) unless admin(event.user)
        return send_invalid_commend_error(event.channel) unless valid_command?(event)
        return send_invalid_channel_error(event.channel) unless edit_afk_message(event)

        Storage::Logs.run.add(raidleader, parser(event.message.content), assists(event.message.content), run_result(event.content))

        event.message.delete
        Senders::Message.new.temp_message(Config.load('run_ended_relay_message'), event.channel)
      end

      def description
        ";endrun [channel number 1-5] [s or f(success or failure)] [leader] [parser] [assist]\n" \
        'This command is used to end the run once post afk has finished. If there was an assist but no parser, replace the parser mention with any text (no spaces).'
      end

      private

      def valid_command?(event)
        return false if raidleader(event.message).nil?
        return false if run_result(event.content).nil?
        return false unless channel_number(event.content)
        return false unless valid_message_length?(event.content)

        true
      end

      def edit_afk_message(event)
        afk_closed_message = format(Config.load('afk_closed_message'), channel(event).name)
        message = Discord::Channel.raid_announcement(event.server).find_bot_message(afk_closed_message)

        return if message.nil?

        message.edit('', embed(event))
      end

      def send_invalid_commend_error(channel)
        Senders::Message.new.temp_message(Config.load('invalid_command_error'), channel)
      end

      def send_invalid_channel_error(channel)
        Senders::Message.new.temp_message(Config.load('no_run_in_channel_error'), channel)
      end

      def valid_message_length?(message)
        message.split.count >= 4
      end

      def embed(event)
        description = format(Config.load('run_ended_message'), channel(event).name)
        footer = Discordrb::Webhooks::EmbedFooter.new(
          text: "Ended by #{@user.nick}",
          icon_url: @user.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          description: description,
          timestamp: Time.now,
          footer: footer
        )

        embed
      end

      def channel(event)
        Discord::Channel.raid_voice(event.server, channel_number(event.content))
      end

      def channel_number(message)
        @channel_number ||= message.split[1]

        begin
          Integer(@channel_number)
        rescue StandardError
          false
        end
      end

      def run_result(message)
        case message.split[2]
        when 's'
          true
        when 'f'
          false
        end
      end

      def raidleader(message = nil)
        @raidleader ||= message.mentions.first&.id
      end

      def parser(message)
        extract_id(message.split(' ')[4])
      end

      def assists(message)
        message.split(' ')[5...].map { |assist| extract_id(assist) }
      end

      def extract_id(string)
        return nil unless string[...3] == '<@!' && string[-1] == '>'

        string[3...-1]
      end

      def admin(user)
        Helpers::User.owner?(user) || Helpers::User.admin?(user) || Helpers::User.head_developer?(user) || Helpers::User.moderator?(user) || Helpers::User.bot?(user)
      end

      def insufficient_perms(event)
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
        event.message.delete
      end
    end
  end
end
