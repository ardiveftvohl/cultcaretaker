# frozen_string_literal: true

require 'discordrb'
require 'time'
require './app/domain/realmeye/character_parser.rb'
require './app/domain/realmeye/api/character.rb'

module Commands
  module BotCommands
    class Whois < Command
      def main!(event)
        return no_user_provided(event.channel) if event.content.split[1]&.downcase.nil?

        event.channel.start_typing
        Senders::Message.new.embed(embed(WhoisHelper.for(event.content)), event.channel)
      end

      def description
        ";whois [username] (class)\n" \
        'This command returns realmeye information of a person'
      end

      private

      def no_user_provided(channel)
        Senders::Message.embed("You didn't provide a username", channel)
      end

      def embed(whois_helper)
        embed = Discordrb::Webhooks::Embed.new(
          title: whois_helper.player_name,
          description: whois_helper.embed_description,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end
    end

    class WhoisHelper
      def self.for(event_content)
        player_name = event_content.split[1]&.downcase
        class_name = event_content.split[2]&.downcase

        @@user_info = Realmeye::Api::User.new(player_name)

        return PlayerOverview.new if class_name.nil?

        ClassOverview.new(class_name)
      end

      def player_name
        @@user_info.player_name
      end
    end

    class PlayerOverview < WhoisHelper
      def embed_description
        description = ''
        @@user_info.characters_data.each do |c|
          description += "#{c['class']} #{c['stats_maxed']}/8\n"
        end
        description
      end
    end

    class ClassOverview < WhoisHelper
      def initialize(class_name)
        @class_name = class_name
      end

      def embed_description
        description = ''
        @@user_info.characters_data.each do |c|
          next unless c['class'].downcase == @class_name

          description += "#{c['class']} #{c['stats_maxed']}/8\n\n"
          maxed_stats = Realmeye::Api::Character.for(@@user_info.player_name, c['class']).maxed_stats
          description += "**Max stats:**\n#{clean(maxed_stats)}\n\n" if c['stats_maxed'].to_i.between?(1, 7)
          description += "**Equipment:**\n#{c['equips']['weapon']} / #{c['equips']['ability']} / #{c['equips']['armor']} / #{c['equips']['ring']}\n\n"
        end
        description
      end

      def clean(stats)
        (stats.map { |stat| parsed(stat) }).join(' / ')
      end

      def parsed(stat)
        return 'Life' if stat == 'hp'
        return 'Mana' if stat == 'mp'

        stat.capitalize
      end
    end
  end
end
