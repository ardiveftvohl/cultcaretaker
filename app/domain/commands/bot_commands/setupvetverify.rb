# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Setupvetverify < Command
      def main!(event)
        return wrong_channel(event) unless event.channel == Discord::Channel.vet_verify(event.server).get
        return insufficient_perms(event) unless role_check(event.user)

        message = Senders::Message.new.embed(setup_embed(event.server), event.channel)
        Senders::Reaction.add_reactions(message, 'malice')
        event.message.delete
      end

      def description
        'This command is only to be used once in #vet-verify to send the reactable verify message'
      end

      private

      def setup_embed(server)
        cultist = Helpers::Emoji.get('cultist', server)
        brain = Helpers::Emoji.get('brain', server)
        grave = Helpers::Emoji.get('grave', server)
        malice = Helpers::Emoji.get('malice', server)
        description = format(Config.load('vet_verification_reaction_message'), cultist, brain, grave, malice)

        embed = Discordrb::Webhooks::Embed.new(
          description: description,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def wrong_channel(event)
        event.message.delete
        Senders::Message.new.temp_message(Config.load('wrong_channel_error'), event.channel)
      end
      
      def insufficient_perms(event)
        event.message.delete
        Senders::Message.new.temp_message(Config.load('insufficient_permissions_error'), event.channel)
      end

      def role_check(user)
        Helpers::User.developer?(user)
      end
    end
  end
end
