# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Finduser < Command
      def main!(event)
        Senders::Message.new.embed(user_embed(event), event.channel)
        event.message.delete
      end

      private

      def user_embed(event)
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "#{event.user.nick} requested a mention of #{target(event.message.content)}",
          icon_url: event.user.avatar_url
        )
        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          description: find_user(event.server),
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def find_user(server)
        user = nil
        server.members.each do |member|
          next if member.nick.nil? || !matched_nickname?(member.nick)

          user = member
          break
        end
        return "Unable to find \"#{target}\"" if user.nil?

        "Found <@!#{user.id}>"
      end

      def matched_nickname?(nickname)
        nicknames = nickname.split(' | ')
        nicknames.each do |n|
          return true if n.downcase.gsub(/[^a-z0-9\s]/i, '') == target.downcase
        end
        false
      end

      def target(message = nil)
        return @target if defined? @target

        @target = message.split(' ')[1]
      end
    end
  end
end
