# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Hc < Command
      def main!(event)
        return unless event.channel.id == Config.load('raid_command_channel').to_i

        user(event)
        message = ghost_message(event.server)
        add_reactions(message)
      end

      def description
        'This command will start a headcount, not an actual afk check'
      end

      private

      def channel(server)
        server.channels.select { |c| c.id == Config.load('raid_anouncement_channel').to_i }.first
      end

      def ghost_message(server)
        message = Senders::Message.new.embed(embed, channel(server), '@here')
        message.edit('', embed)
      end

      def embed
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "Headcount Started by #{user.nick}",
          icon_url: user.avatar_url
        )
        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          description: Config.load('hc_message'),
          timestamp: Time.now
        )

        embed
      end

      def user(event = nil)
        @user ||= event.user
      end

      def add_reactions(message)
        reactions = %w[cultist lost_halls_key knight mseal puri brain]

        Senders::Reaction.add_reactions(message, reactions)
      end
    end
  end
end
