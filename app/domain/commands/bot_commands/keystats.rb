# frozen_string_literal: true

require 'discordrb'
require 'time'

module Commands
  module BotCommands
    class Keystats < Command
      def main!(event)
        return send_no_user_data_error(event.channel) if user(event.message).nil?

        Senders::Message.new.embed(embed(Storage::Logs.key.fetch(user.id), event), event.channel)
        event.message.delete
      end

      def description
        ";keystats [user]\n" \
        'This command shows how many keys someone has popped'
      end

      private

      def send_no_user_data_error(channel)
        Senders::Message.new.temp_message(Config.load('no_user_data_error'), channel)
      end

      def embed(amount, event)
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "Key stats for #{user.nick}",
          icon_url: user.avatar_url
        )
        key_emoji = Helpers::Emoji.get('lost_halls_key', event.server)

        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          description: "#{key_emoji}: #{amount.nil? ? '0' : amount}",
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )

        embed
      end

      def user(message = nil)
        @user ||= message.mentions&.first.on(Config.load('server_id'))
      end
    end
  end
end
