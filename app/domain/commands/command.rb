# frozen_string_literal: true

require 'discordrb'

module Commands
  class Command
    def main!(event)
      event.send_message('This command does not exist')
    end

    def description
      'empty description'
    end
  end
end
