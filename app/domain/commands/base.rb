# frozen_string_literal: true

require 'discordrb'
require_relative 'command_list.rb'

module Commands
  class Base
    def main!(event)
      execute_commands!(event) if event.content[0] == Config.load('prefix')
    end

    private

    def execute_commands!(event)
      command = CommandList.new.command(message(event))

      if command
        command.main!(event)
      else
        event.send_temporary_message Config.load('no_command_error'), Config.load('temp_message_time')
      end
    end

    def message(event)
      event.content.downcase.slice(1..).split.first.downcase.capitalize
    end
  end
end
