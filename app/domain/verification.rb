# frozen_string_literal: true

require_relative 'realmeye/api/user'

class Verification
  class << self
    def minimum?(name, code)
      @user_data = Realmeye::Api::User.new(name)

      return unless description_check?(code)
      return unless private_check
      return unless star_requirement_check

      true
    end

    def description_check?(code)
      retries ||= 0

      @user_data.description.match(/\b(\w*#{code}\w*)\b/) do |c|
        return c[1]
      end

      raise 'no user'
    rescue StandardError
      sleep 5
      retry if (retries += 1) < 6
    end

    def private_check
      return true if @user_data.hidden_location?
    end

    def star_requirement_check
      return true if @user_data.rank >= 15
    end
  end
end
