# frozen_string_literal: true

module Senders
  class Message
    def message(text, channel)
      channel.send_message(text)
    end

    def temp_message(text, channel)
      channel.send_temporary_message(text, Config.load('temp_message_time'))
    end

    def embed(embed, channel, text = '')
      channel.send_embed(text, embed)
    end

    def replace_message(text, message); end

    def replace_emebed(new_embed, embed, text = nil); end

    def pm_message(text, user)
      user.pm(text)
    end

    def pm_embed(embed, user)
      message = user.pm(embed.description)
      message.edit('', embed)
    end
  end
end
