# frozen_string_literal: true

module Senders
  class Reaction
    class << self
      def add_reactions(message, emoji)
        return create_reaction(message, emoji) unless emoji.respond_to?('each')

        emoji.each do |e|
          create_reaction(message, e)
        end
      end

      private

      def create_reaction(message, emoji)
        message.create_reaction(Helpers::Emoji.get(emoji, message.channel.server))
      end
    end
  end
end
