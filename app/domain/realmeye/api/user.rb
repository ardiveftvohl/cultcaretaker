# frozen_string_literal: true

require 'httparty'

module Realmeye
  module Api
    class User
      def initialize(name)
        @user_data = HTTParty.get("https://nightfirec.at/realmeye-api/?player=#{name}")
      end

      def characters_data(character_class = nil)
        characters_data = @user_data['characters']

        character_class ? characters_data.find { |c| c['class'] == character_class } : characters_data
      end

      def description
        [@user_data['desc1'], @user_data['desc2'], @user_data['desc3']].join(' ')
      end

      def desc2
        @user_data['desc2']
      end

      def desc3
        @user_data['desc3']
      end

      def alive_fame
        @user_data['fame']
      end

      def account_fame
        @user_data['account_fame']
      end

      def account_fame_rank
        @user_data['acount_fame_rank']
      end

      def num_chars
        @user_data['chars']
      end

      def guild
        @user_data['guild']
      end

      def guild_rank
        @user_data['guild_rank']
      end

      def player_name
        @user_data['player']
      end

      def hidden_location?
        @user_data['player_last_seen'] == 'hidden'
      end

      def rank
        @user_data['rank'].to_i
      end
    end
  end
end
