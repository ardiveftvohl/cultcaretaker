# frozen_string_literal: true

require 'httparty'

module Realmeye
  module Api
    class Graveyard
      CULT_RUNS_START = "Cultist Hideouts completed</td><td>"
      CULT_RUNS_END = "</td>"
      def initialize(name)
        @graveyard_data = HTTParty.get("https://www.realmeye.com/graveyard-summary-of-player/#{name}").body
      end

      def cult_veteran?
        return false if !defined? @graveyard_data || @graveyard_data.nil?

        n_start = @graveyard_data.index(CULT_RUNS_START) + CULT_RUNS_START.length
        length = @graveyard_data[n_start...].index(CULT_RUNS_END)
        @graveyard_data[n_start..n_start+length].to_i >= 50
      end
    end
  end
end