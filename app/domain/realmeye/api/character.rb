# frozen_string_literal: true

module Realmeye
  module Api
    class Character
      class UnknowStatTypeError < StandardError; end

      EQUIP_TYPES = { weapon: 'weapon', armor: 'armor', ability: 'ability', ring: 'ring' }.freeze
      CLASS_STAT_INFO = YAML.load_file('class_stat_info.yaml')

      def self.for(name, character_class = nil)
        user = User.new(name)

        return unless user.characters_data && !user.characters_data.empty?

        Character.new(character_class ? user.characters_data(character_class) : user.characters_data.first)
      end

      def initialize(character_data)
        @character_data = character_data
      end

      def character_class
        @character_data['class']
      end

      def equips
        equips = {}
        EQUIP_TYPES.each do |key, equip_type|
          equips[key] = equip(equip_type)
        end
        equips
      end

      def weapon
        @character_data['equips']['weapon']
      end

      def armor
        @character_data['equips']['armor']
      end

      def ability
        @character_data['equips']['ability']
      end

      def ring
        @character_data['equips']['ring']
      end

      def maxed_stats
        maxed = []
        CLASS_STAT_INFO[character_class.downcase].keys.filter do |stat|
          maxed << stat if to_max(stat).zero?
        end
        maxed
      end

      def to_max(stat_type)
        stat_max = CLASS_STAT_INFO[character_class.downcase][stat_type]

        raise UnknowStatTypeError unless stat_max
        return 0 if stat(stat_type) >= stat_max

        stat_max - stat(stat_type)
      end

      private

      def stat(stat_type)
        @character_data['stats'][stat_type]
      end

      def equip(item_type)
        return unless EQUIP_TYPES.include?(item_type)

        @character_data['equips'][item_type]
      end
    end
  end
end
