# frozen_string_literal: true

require_relative 'api/user'
require_relative 'api/character'

module Realmeye
  class CharacterParser
    STATS = %w[dexterity attack].freeze
    CLASS_STAT_INFO = YAML.load_file('banned_items.yml')

    def initialize(user_name)
      @character = Api::Character.for(user_name)
    end

    def correct_equipment?
      return false if character.equips.any? { |equip| CLASS_STAT_INFO.include?(equip) }

      true
    end

    def correct_stats?
      STAT.any? { |stat| @character.stat_maxed?(stat) }
    end
  end
end
