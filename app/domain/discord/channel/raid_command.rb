# frozen_string_literal: true

require_relative 'factory.rb'

module Discord
  module Channel
    def self.raid_command(server)
      channel_id = Config.load('raid_command_channel')
      Discord::Channel.for(server, channel_id: channel_id)
    end
  end
end
