# frozen_string_literal: true

require_relative 'factory.rb'

module Discord
  module Channel
    def self.lounge(server)
      channel_id = Config.load('lounge_voice_channel')
      Discord::Channel.for(server, channel_id: channel_id)
    end
  end
end
