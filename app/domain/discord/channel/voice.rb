# frozen_string_literal: true

require_relative 'channel.rb'

module Discord
  module Channel
    class Voice < Channel
      def open(role)
        perms = permissions(role.to_i)
        perms[0].can_connect = true
        perms[1].can_connect = false

        @channel.define_overwrite(role.to_i, *perms)
      end

      def lock(role)
        perms = permissions(role.to_i)
        perms[0].can_connect = false
        perms[1].can_connect = true

        @channel.define_overwrite(role.to_i, *perms)
      end

      def evict_others(people, voice_channel)
        exclude(people).each do |p|
          @channel.server.move(p, voice_channel)
        end
      end

      def move_user_to_self(member)
        @channel.server.move(member, @channel)
      end

      def users
        @channel.users
      end

      private

      def permissions(role)
        allow = @channel.permission_overwrites[role].allow
        deny = @channel.permission_overwrites[role].deny
        [allow, deny]
      end

      def exclude(people)
        vc_members = [*@channel.users]
        vc_members.delete_if { |e| people.map(&:id).include?(e.id) }
        vc_members
      end
    end
  end
end
