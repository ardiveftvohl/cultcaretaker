# frozen_string_literal: true

module Discord
  module Channel
    class Channel
      def initialize(channel)
        @channel = channel
      end

      def id
        @channel.id
      end

      def name
        @channel.name
      end

      def get
        @channel
      end
    end
  end
end
