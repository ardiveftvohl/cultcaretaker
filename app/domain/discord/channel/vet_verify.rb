# frozen_string_literal: true

require_relative 'factory.rb'

module Discord
  module Channel
    def self.vet_verify(server)
      channel_id = Config.load('vet_verify_channel')
      Discord::Channel.for(server, channel_id: channel_id)
    end
  end
end
