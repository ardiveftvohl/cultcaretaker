# frozen_string_literal: true

require_relative 'factory.rb'

module Discord
  module Channel
    def self.vet_raid_voice(server, channel_number)
      channel_id = Config.load("vet_raiding_voice_channel_#{channel_number}")
      Discord::Channel.for(server, channel_id: channel_id)
    end
  end
end
