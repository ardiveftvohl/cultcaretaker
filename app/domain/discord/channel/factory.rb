# frozen_string_literal: true

require_relative 'voice.rb'
require_relative 'text.rb'

module Discord
  module Channel
    class NoChannelArgument < StandardError; end
    class ChannelNotFoundError < StandardError; end
    class ChannelNameNotStringError < StandardError; end
    class ChannelIDNotIntegerError < StandardError; end

    class << self
      def for(server, discord_channel: nil, channel_id: nil, channel_name: nil)
        return channel(discord_channel) if discord_channel
        return channel_for_name(server, channel_name) if channel_name
        return channel_for_id(server, channel_id) if channel_id

        raise NoChannelArgument
      end

      private

      def channel_for_name(server, channel_name)
        raise ChannelNameNotStringError unless channel_name.is_a? String

        server.channels.each do |channel|
          return channel(channel) if channel.name == channel_name
        end

        raise ChannelNotFoundError
      end

      def channel_for_id(server, channel_id)
        channel_id = channel_id.to_i if channel_id.is_a? String

        raise ChannelIDNotIntegerError unless channel_id.is_a? Integer

        server.channels.each do |channel|
          return channel(channel) if channel.id == channel_id
        end

        raise ChannelNotFoundError
      end

      def channel(channel)
        return Voice.new(channel) if channel.voice?

        Text.new(channel)
      end
    end
  end
end
