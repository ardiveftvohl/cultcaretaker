# frozen_string_literal: true

require_relative 'channel.rb'

require 'remove_emoji'

module Discord
  module Channel
    class Text < Channel
      def last_bot_messages(amount = 50)
        last_messages(50).filter(&:from_bot?)[0..amount]
      end

      def last_messages(amount = 50)
        @channel.history(amount)
      end

      def find_bot_message(pattern, search_range = 50)
        match(pattern, last_bot_messages(search_range))
      end

      def find_message(pattern, search_range = 50)
        match(pattern, last_messages(search_range))
      end

      private

      def match(pattern, messages)
        messages.each do |message|
          message_contents(message).each do |content|
            return message if content.match?(/\b(\w*#{sanitize(pattern)}\w*)\b/) || content == sanitize(pattern)
          end
        end
        nil
      end

      def message_contents(message)
        content = []
        content << sanitize(message.content) if message.content
        message.embeds.each do |embed|
          content << sanitize(embed.author.name) if embed.author
          content << sanitize(embed.title) if embed.title
          content << sanitize(embed.description) if embed.description
        end
        content
      end

      def sanitize(sentence)
        RemoveEmoji::Sanitize.call(sentence).rstrip
      end
    end
  end
end
