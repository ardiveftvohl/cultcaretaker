# frozen_string_literal: true

require_relative 'factory.rb'

module Discord
  module Channel
    def self.evidence_submission(server)
      channel_id = Config.load('evidence_submission_channel')
      Discord::Channel.for(server, channel_id: channel_id)
    end
  end
end