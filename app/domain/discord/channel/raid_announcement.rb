# frozen_string_literal: true

require_relative 'factory.rb'

module Discord
  module Channel
    def self.raid_announcement(server)
      channel_id = Config.load('raid_anouncement_channel')
      Discord::Channel.for(server, channel_id: channel_id)
    end
  end
end
