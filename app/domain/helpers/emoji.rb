# frozen_string_literal: true

module Helpers
  class Emoji
    def self.get(emoji, server)
      return '🛑' if emoji == 'stop'
      return '⚠️' if emoji == 'warning'
      return '❌' if emoji == 'x'
      return '🇪🇺' if emoji == 'eu'
      return '🇺🇸' if emoji == 'us'

      server.emoji[Config.load('emoji')[emoji].to_i]
    end
  end
end
