# frozen_string_literal: true

module Helpers
  class User
    class << self
      def assign_role(user, role = Config.load('roles')['verified_raider_role'])
        member(user).add_role(role)
      end

      def assign_nick(user, nick)
        member(user).set_nick(nick)
      end

      def owner?(user)
        User.role?(user, Config.load('roles')['staff']['owner'])
      end

      def admin?(user)
        User.role?(user, Config.load('roles')['staff']['admin'])
      end

      def moderator?(user)
        User.role?(user, Config.load('roles')['staff']['moderator'])
      end

      def head_developer?(user)
        User.role?(user, Config.load('roles')['staff']['head_developer'])
      end

      def developer?(user)
        User.role?(user, Config.load('roles')['staff']['developer'])
      end

      def security?(user)
        User.role?(user, Config.load('roles')['staff']['security'])
      end

      def head_raid_leader?(user)
        User.role?(user, Config.load('roles')['staff']['head_raid_leader'])
      end

      def veteran_raid_leader?(user)
        User.role?(user, Config.load('roles')['staff']['veteran_raid_leader'])
      end

      def raid_leader?(user)
        User.role?(user, Config.load('roles')['staff']['raid_leader'])
      end

      def almost_raid_leader?(user)
        User.role?(user, Config.load('roles')['staff']['almost_raid_leader'])
      end

      def nitro_booster?(user)
        User.role?(user, Config.load('roles')['nitro_booster'])
      end

      def staff?(user)
        User.role?(user, Config.load('roles')['staff'].values)
      end

      def bot?(user)
        User.role?(user, Config.load('roles')['staff']['bot'])
      end

      def official_rusher?(user)
        User.role?(user, Config.load('roles')['official_rusher'])
      end

      def supreme_rusher?(user)
        User.role?(user, Config.load('roles')['supreme_rusher'])
      end

      def superior_knight?(user)
        User.role?(user, Config.load('roles')['superior_knight'])
      end

      def grant_early_loc?(user)
        User.role?(user, Config.load('roles')['loc_whitelist'].values)
      end

      def trial_raid?(user)
        User.role?(user, Config.load('roles')['trial_raid'])
      end

      def role?(user, roles)
        roles = roles.respond_to?(:map) ? roles : [roles]
        roles = roles.map do |r|
          r.is_a?(Discordrb::Role) ? r.id : r
        end

        user.roles.select { |r| roles.include?(r.id.to_s) }.any?
      end

      private

      def member(user)
        user.class == Discordrb::Member ? user : user.on(Config.load('server_id'))
      end
    end
  end
end
