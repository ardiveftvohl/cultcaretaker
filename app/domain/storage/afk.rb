# frozen_string_literal: true

require_relative 'reaction.rb'

module Storage
  class Afk
    def initialize(channel, server, location, rl)
      @@channel = channel
      @@server = server.upcase
      @@location = location.downcase.capitalize
      @@reactions = Reaction.new
      @@rl = rl
    end

    class << self
      def channel(server)
        Discord::Channel.for(server, channel_id: @@channel).get
      end
      
      def server
        @@server
      end

      def location
        @@location
      end

      def rl
        @@rl
      end

      def add_reaction(event)
        @@reactions.push(event)
      end

      def first_reaction(emoji)
        @@reactions.get(emoji)
      end

      def first_reactions(emoji, n)
        @@reactions.get_n(emoji, n)
      end

      def remove_user_reaction(user, emoji)
        @@reactions.pop_user(user, emoji)
      end

      def remove_reaction(emoji)
        @@reactions.pop(emoji)
      end
    end
  end
end
