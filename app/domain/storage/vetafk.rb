# frozen_string_literal: true

require_relative 'reaction.rb'

module Storage
  class Vetafk
    def initialize(channel, server, location)
      @@channel = channel
      @@server = server.upcase
      @@location = location.downcase.capitalize
      @@reactions = Reaction.new
    end

    class << self
      def channel(server)
        Discord::Channel.for(server, channel_id: @@channel).get
      end

      def server
        @@server
      end

      def location
        @@location
      end

      def add_reaction(event)
        @@reactions.push(event)
      end

      def first_reaction(emoji)
        @@reactions.get(emoji)
      end

      def first_reactions(emoji, n)
        @@reactions.get_n(emoji, n)
      end

      def remove_reaction
        @@reactions.pop
      end
    end
  end
end
