# frozen_string_literal: true

module Storage
  class Logs
    class << self
      def key
        Key.new
      end

      def run
        Run.new
      end
    end
  end

  class Storage
    private

    def read(log_path)
      open(log_path, 'r') do |f|
        return JSON.parse(f.read)
      end
    end

    def write(logs, log_path)
      open(log_path, 'w') do |f|
        f.write(logs.to_json)
      end
    end
  end

  class Key < Storage
    LOG_PATH = 'logs/keys.json'

    def add(user, amount = nil)
      logs = read(LOG_PATH)

      logs[user.to_s] = 0 if logs[user.to_s].nil?
      logs[user.to_s] += amount.nil? ? 1 : amount

      write(logs, LOG_PATH)
      logs[user.to_s]
    end

    def fetch(user = nil)
      user.nil? ? read : read(LOG_PATH)[user.to_s]
    end
  end

  class Run < Storage
    LOG_PATH = 'logs/runs.json'

    def add(leader, parser, assists, success)
      @time = Time.now
      @logs = read(LOG_PATH)

      log_run(leader, 'leader', success)
      log_run(parser, 'parser', success) unless parser.nil? 
      assists.each do |assist|
        log_run(assist, 'assist', success)
      end

      write(@logs, LOG_PATH)
      @logs[leader.to_s]
    end

    def fetch(user, from)
      logs = read(LOG_PATH)
      runs = {
        true => {
          'leader' => 0,
          'parser' => 0,
          'assist' => 0
        },
        false => {
          'leader' => 0,
          'parser' => 0,
          'assist' => 0
        }
      }
      return runs if logs[user.to_s].nil?

      logs[user.to_s].reverse_each do |time, run|
        break if !from.nil? && Time.parse(time) < from

        runs[run['success']][run['status']] += 1
      end
      runs
    end

    private

    def log_run(user, status, success)
      @logs[user.to_s] = {} if @logs[user.to_s].nil?
      @logs[user.to_s][@time] = { status: status, success: success}
    end
  end
end
