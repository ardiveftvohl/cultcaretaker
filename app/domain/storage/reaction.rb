# frozen_string_literal: true

module Storage
  class Reaction
    def initialize
      @reactions = []
    end

    def get(emoji)
      @reactions.each do |r|
        return r unless r.nil? || r.emoji != emoji
      end
      nil
    end

    def get_n(emoji, n)
      top_n = []
      @reactions.each do |r|
        break if top_n.length >= n
        top_n << r unless r.nil? || r.emoji != emoji
      end
      top_n
    end

    def push(reaction)
      @reactions = [] if @reactions.nil?

      @reactions << reaction
    end

    def pop_user(user, emoji)
      @reactions.each_with_index do |r, i|
        next if r.nil? || r.user != user || r.emoji != emoji

        return @reactions[i] = nil
      end
    end

    def pop(emoji)
      @reactions.each_with_index do |r, i|
        next if r.nil? || r.emoji != emoji

        return @reactions[i] = nil
      end
    end
  end
end
