# frozen_string_literal: true

require 'discordrb'
require 'time'

module Messages
  class Dashboard
    def initialize(server, bot, rl, rotmg_server, rotmg_bazar, voice_channel_name, voice_channel_number)
      @@server = server
      @@bot = bot
      @@rl = rl
      @@location = "#{rotmg_server} #{rotmg_bazar}"
      @@channel_name = voice_channel_name
      @@channel_number = voice_channel_number

      @@color = '#f8f8f8'
      @@head = ''
      @@key_mention = 'No key reacts'
      @@knight_mentions = 'No knight reacts'
      @@rusher_mentions = 'No rusher reacts'
      @@nitro_mentions = 'No nitro reacts'
      @@parser_mention = 'No one is parsing this run'
      @@assist_mentions = 'No one has taken over this run'
      @@instructions = format(
        Config.load('dashboard_info'),
        Helpers::Emoji.get('malice', @@server), Helpers::Emoji.get('hideout', @@server)
      )
      @@reactions = %w[malice hideout stop warning]
      @@afk_status = 0
    end

    class << self
      def send_message(channel)
        @@message = Senders::Message.new.embed(embed, channel)
        Senders::Reaction.add_reactions(@@message, @@reactions)
      end

      def update(reset_reactions = false)
        @@message.edit('', embed)
        return unless reset_reactions

        @@message.delete_all_reactions
        @@reactions.each do |reaction|
          @@message.create_reaction(reaction)
        end
      end

      def delete
        @@message.delete
      end

      def afk_started?
        return false unless defined? @@afk_status

        @@afk_status == 0
      end

      def afk_ended?
        return false unless defined? @@afk_status

        @@afk_status == 1
      end

      def run_ended?
        return false unless defined? @@afk_status

        @@afk_status == 2
      end

      def color=(color)
        @@color = color
      end

      def head=(head)
        @@head = head
      end

      def instructions
        @@instructions
      end

      def instructions=(instructions)
        @@instructions = instructions
      end

      def reactions=(reactions)
        @@reactions = reactions
      end

      def reset_key(raised_by)
        fake_react = !@@key_mention.include?('No')
        original_key = @@key_mention
        @@key_mention = 'No key reacts'
        return unless fake_react

        transfer_location_to_next_key
        send_warning(raised_by, original_key)
      end

      def key=(key)
        return unless @@key_mention.include?('No')

        @@key_mention = "<@!#{key}>"
      end

      def parser
        @@parser_mention
      end


      def parser=(parser)
        return unless @@parser_mention.include?('No') ^ parser.nil?

        @@parser_mention = parser.nil? ? 'No one is parsing this run' : "<@!#{parser}>"
      end

      def add_assist(assist)
        @@assist_mentions = [] if @@assist_mentions.is_a?(String)
        return if @@assist_mentions.length > 1

        @@assist_mentions << "<@!#{assist}>"
      end

      def remove_assist(assist)
        return if @@assist_mentions.is_a?(String)

        @@assist_mentions.delete("<@!#{assist}>")
        @@assist_mentions = 'No one has taken over this run' if @@assist_mentions.empty?
      end

      def add_knight(knight)
        @@knight_mentions = [] if @@knight_mentions.is_a?(String)

        @@knight_mentions << "<@!#{knight}>"
      end

      def remove_knight(knight)
        return if @@knight_mentions.is_a?(String)

        @@knight_mentions.delete("<@!#{knight}>")
        @@knight_mentions = 'No knight reacts' if @@knight_mentions.empty?
      end
      
      def add_rusher(rusher)
        @@rusher_mentions = [] if @@rusher_mentions.is_a?(String)

        @@rusher_mentions << "<@!#{rusher}>"
      end

      def remove_rusher(rusher)
        return if @@rusher_mentions.is_a?(String)

        @@rusher_mentions.delete("<@!#{rusher}>")
        @@rusher_mentions = 'No rusher reacts' if @@rusher_mentions.empty?
      end

      def add_nitro(nitro)
        @@nitro_mentions = [] if @@nitro_mentions.is_a?(String)

        @@nitro_mentions << "<@!#{nitro}>"
      end

      def remove_nitro(nitro)
        return if @@nitro_mentions.is_a?(String)

        @@nitro_mentions.delete("<@!#{nitro}>")
        @@nitro_mentions = 'No nitro reacts' if @@nitro_mentions.empty?
      end

      def confirm_key(logger)
        return if @@key_mention.include?('No')

        msg = Senders::Message.new.message(
          "#{Config.load('prefix')}logkey #{@@key_mention}",
          Discord::Channel.raid_announcement(@@server).get
        )
        Commands::BotCommands::Logkey.new.main!(
          Discordrb::Events::MessageEvent.new(msg, @@bot), logger
        )
      end

      def end_afk(user)
        @@afk_status = 1
        msg = Senders::Message.new.message(
          "#{Config.load('prefix')}endafk",
          Discord::Channel.raid_command(@@server).get
        )
        Commands::BotCommands::Endafk.new.main!(
          Discordrb::Events::MessageEvent.new(msg, @@bot), user
        )
      end

      def end_run(user, outcome)
        @@afk_status = 2
        cmd_message = "#{Config.load('prefix')}endrun #{@@channel_number} #{outcome} <@!#{user.id}>"
        cmd_message +=  @@parser_mention.include?('No') ? ' nil' : " #{@@parser_mention}"
        cmd_message +=  " #{assist_mentions}" unless assist_mentions.include?('No')
        msg = Senders::Message.new.message(
          cmd_message,
          Discord::Channel.raid_command(@@server).get
        )
        Commands::BotCommands::Endrun.new.main!(
          Discordrb::Events::MessageEvent.new(msg, @@bot), user
        )
      end

      private

      def send_warning(raised_by, user_mention)
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: "Fake reaction raised by #{raised_by.nick}",
          icon_url: raised_by.avatar_url
        )
        warning_embed = Discordrb::Webhooks::Embed.new(
          author: author,
          description: "#{user_mention} has falsly reacted with #{emoji('lost_halls_key')}",
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
        )
        Senders::Message.new.embed(
          warning_embed,
          Discord::Channel.evidence_submission(@@server).get,
          "<@&#{Config.load('roles')['staff']['security']}>"
        )
      end

      def transfer_location_to_next_key
        lh_key = emoji('lost_halls_key')
        Storage::Afk.remove_reaction(lh_key)

        return if Storage::Afk.first_reaction(lh_key).nil?

        msg = Senders::Message.new.pm_embed(
          Discordrb::Webhooks::Embed.new(
            description: Config.load('key_reaction_message'),
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          ),
          Storage::Afk.first_reaction(lh_key).user
        )
        msg.create_reaction('✅')
        msg.create_reaction('❌')
      end

      def emoji(name)
        Helpers::Emoji.get(name, @@server)
      end

      def knight_mentions
        return @@knight_mentions if @@knight_mentions.is_a?(String)

        @@knight_mentions.join(' ')
      end

      def rusher_mentions
        return @@rusher_mentions if @@rusher_mentions.is_a?(String)

        @@rusher_mentions.join(' ')
      end

      def nitro_mentions
        return @@nitro_mentions if @@nitro_mentions.is_a?(String)

        @@nitro_mentions.join(' ')
      end

      def assist_mentions
        return @@assist_mentions if @@assist_mentions.is_a?(String)

        @@assist_mentions.join(' ')
      end

      def description
        desc = ''
        desc += "#{@@head}\n\n"

        desc += "__Essential Raiders__\n"
        desc += "#{emoji('lost_halls_key')} #{@@key_mention}\n"
        desc += "#{emoji('knight')} #{knight_mentions}\n"
        desc += "#{emoji('brain')} #{rusher_mentions}\n\n"

        desc += "*Location is **#{@@location}***\n\n"

        desc += "__Additional reacts__\n"
        desc += "#{emoji('nitro')} #{nitro_mentions}\n"

        desc += "#{emoji('malice')} #{@@parser_mention}\n"
        desc += "#{emoji('staff_cultist')} #{assist_mentions}\n\n"

        desc += @@instructions.to_s
        desc
      end

      def embed
        author = Discordrb::Webhooks::EmbedAuthor.new(
          name: format(Config.load('dashboard_title'), @@rl.nick, @@channel_name),
          icon_url: @@rl.avatar_url
        )

        embed = Discordrb::Webhooks::Embed.new(
          author: author,
          description: description,
          footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}"),
          color: @@color
        )

        embed
      end
    end
  end
end
