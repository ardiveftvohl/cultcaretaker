# frozen_string_literal: true

require 'open3'
require 'rmagick'

module Processors
  WHO_MARKER = 'Players online ('
  STD_FUZZ = 4_883
  COLOR_MARKER = Magick::Pixel.new(65_535, 0, 0)
  ROTMG_YELLLOW = Magick::Pixel.new(58_596, 56_837, 19_216)
  class Image
    def initialize(filename)
      @img_path = filename
      @img = Magick::Image.read(filename).first
      @w = @img.columns
      @h = @img.rows
      @img.fuzz = STD_FUZZ
    end

    def process
      @img = remove_background(filter_yellow(crop(@img)))
      update_file
      return "Screenshot does not contain the '/who' command." unless raw_text.include? WHO_MARKER

      start = raw_text.index(WHO_MARKER) + WHO_MARKER.length + 5
      extract_igns(raw_text, start)
    end

    private

    def extract_igns(text, start)
      text[start..].gsub(',', ' ').gsub(/\s+/, ' ').split(' ')
    end

    def raw_text
      text ||= Open3.capture3("tesseract #{@img_path} stdout -l eng --oem 1").first.strip
    end

    def crop(img)
      img.crop(0, 0.65 * @h, 0.75 * @w, 0.35 * @h)
    end

    def filter_yellow(img)
      img.opaque_channel(ROTMG_YELLLOW, COLOR_MARKER)
         .opaque_channel('yellow', COLOR_MARKER)
    end

    def remove_background(img)
      img.opaque_channel(COLOR_MARKER, 'black', invert: true)
         .opaque_channel(COLOR_MARKER, 'white')
    end

    def update_file
      @img.write(@img_path)
    end
  end
end
