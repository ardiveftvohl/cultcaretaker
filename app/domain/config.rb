# frozen_string_literal: true

require 'discordrb'
require 'yaml'

CONFIG = YAML.load_file('settings.yaml')

class Config
  class << self
    def load(name)
      return dev_config[name] if ENV['BOT_ENV'] == 'development'

      prod_config[name]
    end

    def dev_config
      CONFIG.merge(YAML.load_file('settings_development.yaml'))
    end

    def prod_config
      CONFIG.merge(YAML.load_file('settings_production.yaml'))
    end
  end
end
