# frozen_string_literal: true

require 'discordrb'

module Actions
  module ReactionRemove
    class Base
      def initialize
        load_receivers
      end

      def main!(event)
        return unless event.message.from_bot?

        return dashboard_reaction(event) if dashboard_message?(event)
        return afk_reaction_remove(event) if afk_message?(event)
      end

      private

      def dashboard_message?(event)
        return if event.message.embeds.first.author.nil?

        event.message.embeds.first.author.name&.match?(/\b(\w*Dashboard for\w*)\b/)
      end

      def dashboard_reaction(event)
        ReactionRemove::BotActions::DashboardReactionRemove.new.main!(event)
      end

      def afk_message?(event)
        return if event.message.embeds.first.author.nil?

        event.message.embeds.first.author.name&.match?(/\b(\w*AFK Check Started\w*)\b/)
      end

      def afk_reaction_remove(event)
        ReactionRemove::BotActions::AfkReactionRemove.new.main!(event)
      end

      def load_receivers
        @@load_receivers ||= Dir["#{File.dirname(__FILE__)}/bot_actions/**/*.rb"].each do |file|
          require File.absolute_path(file)
          File.basename(file, '.*')
        end
      end
    end
  end
end
