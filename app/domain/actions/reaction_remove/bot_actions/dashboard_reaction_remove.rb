# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module ReactionRemove
    module BotActions
      class DashboardReactionRemove
        def main!(event)
          return unless role_check(event.user)

          if event.emoji == emoji('malice', event.server)
            Messages::Dashboard.parser = nil if Messages::Dashboard.parser == "<@!#{event.user.id}>"
            Messages::Dashboard.update
          end

          if event.emoji == emoji('staff_cultist', event.server)
            Messages::Dashboard.remove_assist(event.user.id)
            Messages::Dashboard.update
          end
        end

        private

        def emoji(name, server)
          Helpers::Emoji.get(name, server)
        end

        def role_check(user)
          Helpers::User.staff?(user)
        end
      end
    end
  end
end
