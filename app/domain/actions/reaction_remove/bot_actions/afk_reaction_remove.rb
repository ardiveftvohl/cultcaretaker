# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module ReactionRemove
    module BotActions
      class AfkReactionRemove
        def main!(event)
          @server = event.server
          updatable_reacts = [
            emoji('knight', event.server), emoji('brain', event.server),
            emoji('nitro', event.server)
          ]
          return unless updatable_reacts.include?(event.emoji)

          update_dashboard(event.emoji, event.user.id)
        end

        private

        def emoji(name, server)
          Helpers::Emoji.get(name, server)
        end

        def update_dashboard(emoji, user_id)
          Messages::Dashboard.remove_knight(user_id) if emoji == emoji('knight', @server)
          Messages::Dashboard.remove_rusher(user_id) if emoji == emoji('brain', @server)
          Messages::Dashboard.remove_nitro(user_id) if emoji == emoji('nitro', @server)
          Messages::Dashboard.update
        end
      end
    end
  end
end
