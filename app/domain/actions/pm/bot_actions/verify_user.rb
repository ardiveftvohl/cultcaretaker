# frozen_string_literal: true

require 'discordrb'
require 'time'
require './app/domain/verification.rb'

module Actions
  module Pm
    module BotActions
      class VerifyUser
        def main!(event)
          @bot_icon = event.bot.profile.avatar_url
          Senders::Message.new.embed(embed(Config.load('verification_wait_message')), event.channel)

          if Verification.minimum?(user_name(event.channel), user_code(event))
            begin
              Helpers::User.assign_nick(event.author, user_name(event.channel).downcase)
              Helpers::User.assign_role(event.author)
            rescue Discordrb::Errors::NoPermission
            end
            Senders::Message.new.embed(embed(success_description), event.channel)
          else
            Senders::Message.new.embed(embed(Config.load('verification_failure_message')), event.channel)
          end
        end

        private

        def embed(description)
          Discordrb::Webhooks::Embed.new(
            author: Discordrb::Webhooks::EmbedAuthor.new(name: 'Malice', icon_url: @bot_icon),
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )
        end

        def user_name(channel)
          Discord::Channel.for(channel.server, discord_channel: channel).last_messages(2)[1].content
        end

        def user_code(event)
          message = Discord::Channel.for(event.server, discord_channel: event.channel).find_bot_message('MALICE')

          message.embeds.first.description&.match(/\b(\w*MALICE\w*)\b/) do |code|
            return code[1]
          end
        end

        def success_description
          format(Config.load('verification_accept_message'), "<##{Config.load('raiding_rules')}>")
        end
      end
    end
  end
end
