# frozen_string_literal: true

require 'discordrb'

module Actions
  module Pm
    class Base
      def initialize
        load_receivers
      end

      def main!(event)
        @channel = Discord::Channel.for(event.server, discord_channel: event.channel)
        return if @channel.last_messages(1).first.from_bot?

        return verification(event) if verification_message?
      end

      private

      def verification(event)
        Pm::BotActions::VerifyUser.new.main!(event)
      end

      def verification_message?
        return true if @channel.find_bot_message(Config.load('verification_start_message')[0...20], 1)
        return true if @channel.find_bot_message(Config.load('verification_failure_message')[0...20], 1)

        false
      end

      def load_receivers
        @@load_receivers ||= Dir["#{File.dirname(__FILE__)}/bot_actions/**/*.rb"].each do |file|
          require File.absolute_path(file)
          File.basename(file, '.*')
        end
      end
    end
  end
end
