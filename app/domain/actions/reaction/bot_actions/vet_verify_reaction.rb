# frozen_string_literal: true

require 'discordrb'
require 'time'
require './app/domain/vet_verification.rb'

module Actions
  module Reaction
    module BotActions
      class VetVerifyReaction
        def main!(event)
          @bot_icon = event.bot.profile.avatar_url
          if VetVerification.minimum?(name(event.user))
            begin
              Helpers::User.assign_role(event.user, Config.load('roles')['veteran_raider_role'])
            rescue Discordrb::Errors::NoPermission
            end
            Senders::Message.new.pm_embed(embed(success_description), event.user)
          else
            Senders::Message.new.pm_embed(embed(failure_description), event.user)
          end
        end

        private

        def name(user)
          user.nick.nil? ? user.username.gsub(/[^\w\d]/, '') : user.nick.gsub(/[^\w\d]/, '')
        end

        def embed(description)
          Discordrb::Webhooks::Embed.new(
            author: Discordrb::Webhooks::EmbedAuthor.new(name: 'Malice', icon_url: @bot_icon),
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )
        end

        def success_description
          format(Config.load('vet_verification_accept_message'), "<##{Config.load('vet_requirements')}>")
        end

        def failure_description
          format(Config.load('vet_verification_failure_message'), "<##{Config.load('vet_verify_channel')}>")
        end
      end
    end
  end
end
