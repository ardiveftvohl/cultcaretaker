# frozen_string_literal: true

require 'discordrb'
require 'httparty'
require 'time'

module Actions
  module Reaction
    module BotActions
      class VetAfkReaction
        def main!(event)
          @bot_icon = event.bot.profile.avatar_url

          return VetKey.new.main!(event) if event.emoji == emoji('lost_halls_key', event.server)
          return VetNitro.new.main!(event) if event.emoji == emoji('nitro', event.server)
          return VetAbort.new.main!(event) if event.emoji.name == emoji('x', event.server)
          return VetEarlyLocRole.new.main!(event, 'knight') if event.emoji == emoji('knight', event.server)
          return VetEarlyLocRole.new.main!(event, 'rusher') if event.emoji == emoji('brain', event.server)

          VetPortal.new.main!(event) if event.emoji == emoji('cultist', event.server)
        end

        private

        def embed(description)
          embed = Discordrb::Webhooks::Embed.new(
            author: Discordrb::Webhooks::EmbedAuthor.new(name: 'Malice - Cultist Hideout', icon_url: @bot_icon),
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )

          embed
        end

        def emoji(name, server)
          Helpers::Emoji.get(name, server)
        end
      end

      class VetPortal < VetAfkReaction
        def main!(event)
          return unless role_check(event.user)

          msg = "#{Config.load('staff_reaction_message')} `#{Storage::Vetafk.server} #{Storage::Vetafk.location}`"
          Senders::Message.new.pm_embed(embed(msg), event.user)
        end

        private

        def role_check(user)
          Helpers::User.staff?(user) || Helpers::User.grant_early_loc?(user)
        end
      end

      class VetKey < VetAfkReaction
        def main!(event)
          Storage::Vetafk.add_reaction(event)
          return unless first_reaction?(event)

          message = Senders::Message.new.pm_embed(embed(Config.load('vet_key_reaction_message')), event.user)
          add_reactions(message)
        end

        private

        def first_reaction?(event)
          Storage::Vetafk.first_reaction(event.emoji) == event
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end

      class VetEarlyLocRole < VetAfkReaction
        def main!(event, role)
          Storage::Vetafk.add_reaction(event) if role_check(event.user, role)
          return unless first_reactions?(event, 3)

          message = Senders::Message.new.pm_embed(embed(Config.load("#{role}_reaction_message")), event.user)
          add_reactions(message)
        end

        private

        def role_check(user, role)
          return Helpers::User.superior_knight?(user) if role == 'knight'

          Helpers::User.official_rusher?(user) || Helpers::User.supreme_rusher?(user)
        end

        def first_reactions?(event, n)
          Storage::Vetafk.first_reactions(event.emoji, n).include?(event)
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end

      class VetNitro < VetAfkReaction
        def main!(event)
          return unless role_check(event.user)

          msg = "#{Config.load('nitro_reaction_message')} `#{Storage::Vetafk.server} #{Storage::Vetafk.location}`"
          Senders::Message.new.pm_embed(embed(msg), event.user)
        end

        private

        def role_check(user)
          Helpers::User.nitro_booster?(user)
        end
      end

      class VetAbort < VetAfkReaction
        def main!(event)
          return unless role_check(event.user)

          lock_raiding_channel(event)

          event.message.delete_all_reactions
          event.message.edit('', abort_embed(event.user, afk_abort_text(event.server)))
        end

        private

        def afk_abort_text(server)
          format(Config.load('abort_reaction_message'), Storage::Vetafk.channel(server).name)
        end

        def lock_raiding_channel(event)
          raiding_channel(event).lock(Config.load('roles')['veteran_raider_role'])
        end

        def raiding_channel(event)
          channel_number = event.message.embeds.first.author.name.match(/Veteran Raiding (\d)/)[1]
          Discord::Channel.raid_voice(event.server, channel_number)
        end

        def role_check(user)
          Helpers::User.staff?(user)
        end

        def abort_embed(user, description)
          footer = Discordrb::Webhooks::EmbedFooter.new(
            text: "Aborted by #{user.nick}",
            icon_url: user.avatar_url
          )
          embed = Discordrb::Webhooks::Embed.new(
            description: description,
            timestamp: Time.now,
            footer: footer
          )
          embed
        end
      end
    end
  end
end
