# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class AfkConfirmation
        def main!(event)
          event.message.delete
          @lh_key = Helpers::Emoji.get('lost_halls_key', server(event))

          case event.emoji.name
          when '✅'
            check_mark_response(event)
          when '❌'
            cross_response(event)
          end
        end

        private

        def embed(description)
          Discordrb::Webhooks::Embed.new(
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )
        end

        def check_mark_response(event)
          msg = "#{Config.load('react_confirm_message')} `#{Storage::Afk.server} #{Storage::Afk.location}`"

          Senders::Message.new.embed(embed(msg), event.channel)
          Messages::Dashboard.key = event.user.id
          Messages::Dashboard.update
        end

        def cross_response(event)
          Senders::Message.new.embed(embed(Config.load('key_deny_message')), event.channel)
          Storage::Afk.remove_user_reaction(event.user, @lh_key)

          return if Storage::Afk.first_reaction(@lh_key).nil?

          msg = Senders::Message.new.pm_embed(
            embed(Config.load('key_reaction_message')),
            Storage::Afk.first_reaction(@lh_key).user
          )
          add_reactions(msg)
        end

        def server(event = nil)
          return @server if defined? @server

          @server = event.bot.servers[Config.load('server_id').to_i]
        end

        def nickname
          Storage::Afk.first_reaction(@lh_key).user.nick
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end
    end
  end
end
