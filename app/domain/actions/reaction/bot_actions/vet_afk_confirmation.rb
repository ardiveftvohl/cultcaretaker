# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class VetAfkConfirmation
        def main!(event)
          event.message.delete
          @lh_key = Helpers::Emoji.get('lost_halls_key', server(event))

          case event.emoji.name
          when '✅'
            check_mark_response(event)
          when '❌'
            cross_response(event)
          end
        end

        private

        def embed(description)
          Discordrb::Webhooks::Embed.new(
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )
        end

        def check_mark_response(event)
          msg = "#{Config.load('react_confirm_message')} `#{Storage::Vetafk.server} #{Storage::Vetafk.location}`"

          Senders::Message.new.embed(embed(msg), event.channel)
          send_key_confirmation_query(event)
        end

        def cross_response(event)
          Senders::Message.new.embed(embed(Config.load('key_deny_message')), event.channel)
          emoji = Helpers::Emoji.get(role == 'knight' ? 'knight' : 'brain', event.server)
          Storage::Vetafk.remove_reaction(event.user, emoji)

          return if Storage::Vetafk.first_reaction(@lh_key).nil?

          msg = Senders::Message.new.pm_embed(
            embed(Config.load('key_reaction_message')),
            Storage::Vetafk.first_reaction(@lh_key).user
          )
          add_reactions(msg)
        end

        def send_key_confirmation_query(event)
          message = Senders::Message.new.embed(
            staff_key_confirmed_embed(server(event)),
            Discord::Channel.vet_raid_command(server(event)).get
          )
          add_reactions(message)
        end

        def staff_key_confirmed_embed(server)
          title = "Key confirmation"
          description = format(
            Config.load('vet_key_confirmed_logger'),
            nickname, @lh_key
          )

          embed = Discordrb::Webhooks::Embed.new(
            title: title,
            description: description
          )

          embed
        end

        def server(event)
          @server ||= event.bot.servers[Config.load('server_id').to_i]
        end

        def nickname
          Storage::Vetafk.first_reaction(@lh_key).user.nick
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end
    end
  end
end
