# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class VetKeyConfirmation
        def main!(event)
          clear_key_confirmation_message(event.server)
          @lh_key = Helpers::Emoji.get('lost_halls_key', event.server)

          case event.emoji.name
          when '✅'
            check_mark_response(event)
          when '❌'
            cross_response(event)
          end
        end

        private

        def embed(description)
          Discordrb::Webhooks::Embed.new(description: description)
        end

        def check_mark_response(event)
          Senders::Message.new.embed(true_react_embed(event), event.channel)
          msg = Senders::Message.new.message(logkey_message(), Discord::Channel.vet_raid_announcement(event.server).get)
          trigger_logkey(msg, event.bot, event.user)
        end

        def trigger_logkey(msg, bot, logger)
          logkey_event = Discordrb::Events::MessageEvent.new(msg, bot)
          Commands::BotCommands::Logkey.new.main!(logkey_event, logger)
        end

        def logkey_message
          "#{Config.load('prefix')}logkey <@!#{Storage::Vetafk.first_reaction(@lh_key).user.id}>"
        end

        def true_react_embed(event)
          title = '✅ Key Confirmed ✅'
          desc = "Thank you for confirming #{Storage::Vetafk.first_reaction(@lh_key).user.nickname}'s #{@lh_key}"
          footer = Discordrb::Webhooks::EmbedFooter.new(
            text: "Confirmed by #{event.user.nick}",
            icon_url: event.user.avatar_url
          )

          embed = Discordrb::Webhooks::Embed.new(
            title: title,
            description: desc,
            timestamp: Time.now,
            footer: footer
          )

          embed
        end

        def cross_response(event)
          Senders::Message.new.embed(fake_react_embed(event), event.channel)
          Storage::Vetafk.remove_reaction(event.user, @lh_key)

          return if Storage::Vetafk.first_reaction(@lh_key).nil?

          msg = Senders::Message.new.pm_embed(
            embed(Config.load('key_reaction_message')),
            Storage::Vetafk.first_reaction(@lh_key).user
          )
          add_reactions(msg)
        end

        def fake_react_embed(event)
          title = '❌ Fake Reaction Detected ❌'
          desc = "#{Storage::Vetafk.first_reaction(@lh_key).user.nickname} falsly reacted with #{@lh_key}"
          footer = Discordrb::Webhooks::EmbedFooter.new(
            text: "Raised by #{event.user.nick}",
            icon_url: event.user.avatar_url
          )

          embed = Discordrb::Webhooks::Embed.new(
            title: title,
            description: desc,
            timestamp: Time.now,
            footer: footer
          )

          embed
        end

        def clear_key_confirmation_message(server)
          last_key_confirmation(server).delete
        end

        def last_key_confirmation(server)
          Discord::Channel.vet_raid_command(server).find_bot_message('Key confirmation')
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end
    end
  end
end
