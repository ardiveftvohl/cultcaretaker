# frozen_string_literal: true

require 'discordrb'

module Actions
  module Reaction
    module BotActions
      class AfkPostReaction
        def main!(event)
          move_user(event) if cultitst_reaction(event)
        end

        def move_user(event)
          return if event.user.voice_channel.nil?

          raiding_channel(event).move_user_to_self(event.user)
        end

        def cultitst_reaction(event)
          event.emoji == Helpers::Emoji.get('cultist', event.server)
        end

        def raiding_channel(event)
          Discord::Channel.raid_voice(event.server, channel_number(event))
        end

        def channel_number(event)
          event.message.embeds.first.description.match(/Raiding (\d)/)[1]
        end
      end
    end
  end
end
