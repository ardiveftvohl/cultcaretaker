# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class DashboardReaction
        def main!(event)
          return unless role_check(event.user)

          if event.emoji == emoji('malice', event.server)
            Messages::Dashboard.parser = event.user.id
            Messages::Dashboard.update
            return
          end

          if event.emoji.name == emoji('warning', event.server)
            Messages::Dashboard.reset_key(event.user)
            Messages::Dashboard.update
            return
          end
          
          if event.emoji == emoji('staff_cultist', event.server)
            Messages::Dashboard.add_assist(event.user.id)
            Messages::Dashboard.update
            return
          end

          return Abort.new.main!(event) if event.emoji.name == emoji('stop', event.server)

          EndAfk.new.main!(event) if event.emoji == emoji('hideout', event.server)
          ConfKey.new.main!(event) if event.emoji == emoji('lost_halls_key', event.server)
          EndRun.new.main!(event) if ['✅', '❌'].include?(event.emoji.name)

          Messages::Dashboard.update(true)
        end

        private

        def emoji(name, server)
          Helpers::Emoji.get(name, server)
        end

        def role_check(user)
          Helpers::User.staff?(user)
        end
      end

      class Abort < DashboardReaction
        def main!(event)
          return unless role_check(event.user)

          lock_raiding_channel(event)
          
          Messages::Dashboard.delete

          afk_message = last_afk_message(event.server)
          afk_message.delete_all_reactions
          afk_message.edit('', abort_embed(event.user, afk_abort_text(event.server)))
        end

        private

        def last_afk_message(server)
            Discord::Channel.raid_announcement(server).find_bot_message('AFK Check Started')
        end

        def afk_abort_text(server)
          format(Config.load('abort_reaction_message'), Storage::Afk.channel(server).name)
        end

        def lock_raiding_channel(event)
          raiding_channel(event).lock(Config.load('roles')['verified_raider_role'])
        end

        def raiding_channel(event)
          channel_number = event.message.embeds.first.author.name.match(/Raiding (\d)/)[1]
          Discord::Channel.raid_voice(event.server, channel_number)
        end

        def abort_embed(user, description)
          footer = Discordrb::Webhooks::EmbedFooter.new(
            text: "Aborted by #{user.nick}",
            icon_url: user.avatar_url
          )
          embed = Discordrb::Webhooks::Embed.new(
            description: description,
            timestamp: Time.now,
            footer: footer
          )
          embed
        end

        def role_check(user)
          Helpers::User.staff?(user)
        end
      end

      class EndAfk < DashboardReaction
        def main!(event)
          return unless role_check(event.user)

          Messages::Dashboard.reactions = []
          Messages::Dashboard.instructions = ''
          Messages::Dashboard.color = '#ffbb00'
          Messages::Dashboard.head = '**This AFK-check has ended**'
          Messages::Dashboard.update(true)
          Messages::Dashboard.end_afk(event.user)

          lh = emoji('lost_halls_key', event.server)
          Messages::Dashboard.color = '#ff0000'
          Messages::Dashboard.instructions = "React with #{lh} to log the key"
          Messages::Dashboard.reactions = [lh]
        end
      end

      class ConfKey < DashboardReaction
        def main!(event)
          return unless role_check(event.user)

          assist_emoji = emoji('staff_cultist', event.server)
          instructions = "If this run was a success react with ✅\n"
          instructions += "otherwise react with ❌\n"
          instructions += "React with #{assist_emoji} if you have taken over this run"
          Messages::Dashboard.confirm_key(event.user)
          Messages::Dashboard.color = '#ff0000'
          Messages::Dashboard.reactions = ['✅', '❌', assist_emoji]
          Messages::Dashboard.instructions = instructions
        end
      end

      class EndRun < DashboardReaction
        def main!(event)
          return unless role_check(event.user)

          case event.emoji.name
          when '✅'
            success_fail = 's'
          when '❌'
            success_fail = 'f'
          end
          
          Messages::Dashboard.reactions = []
          Messages::Dashboard.head = '**This run has ended**'
          Messages::Dashboard.color = '#202225'
          Messages::Dashboard.instructions = ''
          Messages::Dashboard.end_run(event.user, success_fail)
        end
      end
    end
  end
end
