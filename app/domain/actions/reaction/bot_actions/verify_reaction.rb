# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class VerifyReaction
        def main!(event)
          @bot_icon = event.bot.profile.avatar_url
          Senders::Message.new.pm_embed(embed(format(Config.load('verification_start_message'), code(8))), event.user)
        end

        private

        def embed(description)
          Discordrb::Webhooks::Embed.new(
            author: Discordrb::Webhooks::EmbedAuthor.new(name: 'Malice', icon_url: @bot_icon),
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )
        end

        def code(length)
          'MALICE' + (0...length).map { ('a'..'z').to_a[rand(26)] }.join.upcase
        end
      end
    end
  end
end
