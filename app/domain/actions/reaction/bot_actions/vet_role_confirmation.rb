# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class VetRoleConfirmation
        def main!(event, role)
          event.message.delete
          @role = role

          case event.emoji.name
          when '✅'
            check_mark_response(event)
          when '❌'
            cross_response(event)
          end
        end

        private

        def embed(description)
          embed = Discordrb::Webhooks::Embed.new(
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )

          embed
        end

        def check_mark_response(event)
          msg = "#{Config.load('react_confirm_message')} `#{Storage::Vetafk.server} #{Storage::Vetafk.location}`"

          Senders::Message.new.embed(embed(msg), event.channel)
        end

        def cross_response(event)
          Senders::Message.new.embed(embed(Config.load('key_deny_message')), event.channel)
          emoji = Helpers::Emoji.get(role == 'knight' ? 'knight' : 'brain', event.server)
          Storage::Vetafk.remove_reaction(event.user, emoji)

          return if Storage::Vetafk.first_reactions(@role, 3).length < 3

          Storeage::Afk.first_reactions(@role, 3).each do |r|
            next if r.nil? || already_contacted(r.user)
            msg = Senders::Message.new.pm_embed(embed(Config.load("vet_#{@role}_reaction_message")), r.user)
            add_reactions(msg)
          end
        end

        def already_contacted(user)
            last_bot_message = Discord::Channel::Text.new(user.pm).last_bot_messages(1)
            return false if last_bot_message.nil?

            (Time.now - last_bot_message.timestamp) <= 10*60
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end
    end
  end
end
