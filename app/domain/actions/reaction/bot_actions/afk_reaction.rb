# frozen_string_literal: true

require 'discordrb'
require 'time'

module Actions
  module Reaction
    module BotActions
      class AfkReaction
        def main!(event)
          @bot_icon = event.bot.profile.avatar_url

          return Key.new.main!(event) if event.emoji == emoji('lost_halls_key', event.server)
          return Nitro.new.main!(event) if event.emoji == emoji('nitro', event.server)
          return EarlyLocRole.new.main!(event, 'knight') if event.emoji == emoji('knight', event.server)
          return EarlyLocRole.new.main!(event, 'rusher') if event.emoji == emoji('brain', event.server)

          Portal.new.main!(event) if event.emoji == emoji('cultist', event.server)
        end

        private

        def embed(description)
          embed = Discordrb::Webhooks::Embed.new(
            author: Discordrb::Webhooks::EmbedAuthor.new(name: 'Malice - Cultist Hideout', icon_url: @bot_icon),
            description: description,
            footer: Discordrb::Webhooks::EmbedFooter.new(text: "© Malice #{Time.now.year}")
          )

          embed
        end

        def emoji(name, server)
          Helpers::Emoji.get(name, server)
        end
      end

      class Portal < AfkReaction
        def main!(event)
          return unless role_check(event.user)

          msg = "#{Config.load('staff_reaction_message')} `#{Storage::Afk.server} #{Storage::Afk.location}`"
          Senders::Message.new.pm_embed(embed(msg), event.user)
        end

        private

        def role_check(user)
          Helpers::User.staff?(user) || Helpers::User.grant_early_loc?(user)
        end
      end

      class Key < AfkReaction
        def main!(event)
          Storage::Afk.add_reaction(event)
          return unless first_reaction?(event)

          message = Senders::Message.new.pm_embed(embed(Config.load('key_reaction_message')), event.user)
          add_reactions(message)
        end

        private

        def first_reaction?(event)
          Storage::Afk.first_reaction(event.emoji) == event
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end

      class EarlyLocRole < AfkReaction
        def main!(event, role)
          Storage::Afk.add_reaction(event) if role_check(event.user, role)
          return unless first_reactions?(event, 3)

          message = Senders::Message.new.pm_embed(embed(Config.load("#{role}_reaction_message")), event.user)
          add_reactions(message)
        end

        private

        def role_check(user, role)
          return Helpers::User.superior_knight?(user) if role == 'knight'

          Helpers::User.official_rusher?(user) || Helpers::User.supreme_rusher?(user)
        end

        def first_reactions?(event, n)
          Storage::Afk.first_reactions(event.emoji, n).include?(event)
        end

        def add_reactions(message)
          message.create_reaction('✅')
          message.create_reaction('❌')
        end
      end

      class Nitro < AfkReaction
        def main!(event)
          return unless role_check(event.user)

          msg = "#{Config.load('nitro_reaction_message')} `#{Storage::Afk.server} #{Storage::Afk.location}`"
          Senders::Message.new.pm_embed(embed(msg), event.user)
          Messages::Dashboard.add_nitro(event.user.id)
          Messages::Dashboard.update
        end

        private

        def role_check(user)
          Helpers::User.nitro_booster?(user)
        end
      end
    end
  end
end
