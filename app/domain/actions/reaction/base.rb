# frozen_string_literal: true

require 'discordrb'

module Actions
  module Reaction
    class Base
      def initialize
        load_receivers
      end

      def main!(event)
        return unless event.message.from_bot?

        return dashboard_reaction(event) if dashboard_message?(event)
        return afk_reaction(event) if afk_message?(event)
        return vet_afk_reaction(event) if vet_afk_message?(event)
        return afk_post_reaction(event) if afk_post_message?(event)
        return vet_afk_post_reaction(event) if vet_afk_post_message?(event)
        return vet_key_confirmed_reaction(event) if vet_key_confirmed_message?(event)

        forward_reaction_message(event)
      end

      private

      def forward_reaction_message(event)
        description = event.message.embeds.first.description[0...30]
        class_mapping = {
          'rusher' => Reaction::BotActions::RoleConfirmation,
          'vet_rusher' => Reaction::BotActions::VetRoleConfirmation,
          'knight' => Reaction::BotActions::RoleConfirmation,
          'vet_knight' => Reaction::BotActions::VetRoleConfirmation,
          'key' => Reaction::BotActions::AfkConfirmation,
          'vet_key' => Reaction::BotActions::VetAfkConfirmation,
          'verification' => Reaction::BotActions::VerifyReaction,
          'vet_verification' => Reaction::BotActions::VetVerifyReaction
        }

        class_mapping.each do |keyword, _class|
          next unless description == Config.load("#{keyword}_reaction_message")[0...30]

          if keyword.include?('key') || keyword.include?('verification')
            _class.new.main!(event)
          else
            keyword.slice!('vet_') if keyword.include?('vet')
            _class.new.main!(event, keyword)
          end
        end
      end

      def vet_key_confirmed_message?(event)
        event.message.embeds.first.description[-20...] == Config.load('vet_key_confirmed_logger')[-20...]
      end

      def vet_key_confirmed_reaction(event)
        Reaction::BotActions::VetKeyConfirmation.new.main!(event)
      end

      def dashboard_message?(event)
        return if event.message.embeds.first.author.nil?

        event.message.embeds.first.author.name&.match?(/\b(\w*Dashboard for\w*)\b/)
      end

      def dashboard_reaction(event)
        Reaction::BotActions::DashboardReaction.new.main!(event)
      end

      def afk_message?(event)
        return if event.message.embeds.first.author.nil?

        event.message.embeds.first.author.name&.match?(/\b(\w*AFK Check Started\w*)\b/)
      end

      def afk_reaction(event)
        Reaction::BotActions::AfkReaction.new.main!(event)
      end

      def afk_post_message?(event)
        event.message.embeds.first.description[0...20] == Config.load('afk_end_message')[0...20]
      end

      def afk_post_reaction(event)
        Reaction::BotActions::AfkPostReaction.new.main!(event)
      end

      def vet_afk_message?(event)
        return if event.message.embeds.first.author.nil?

        event.message.embeds.first.author.name&.match?(/\b(\w*AFK Check Started for a\w*)\b/)
      end

      def vet_afk_reaction(event)
        Reaction::BotActions::VetAfkReaction.new.main!(event)
      end
      
      def vet_afk_post_message?(event)
        event.message.embeds.first.description[0...20] == Config.load('vet_afk_end_message')[0...20]
      end

      def vet_afk_post_reaction(event)
        Reaction::BotActions::VetAfkPostReaction.new.main!(event)
      end

      def load_receivers
        @@load_receivers ||= Dir["#{File.dirname(__FILE__)}/bot_actions/**/*.rb"].each do |file|
          require File.absolute_path(file)
          File.basename(file, '.*')
        end
      end
    end
  end
end
