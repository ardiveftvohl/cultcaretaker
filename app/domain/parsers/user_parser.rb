# frozen_string_literal: true

require 'discordrb'
require './app/domain/realmeye/api/character'

module Parsers
  class UserParser
    BANNED_EQUIPMENT = YAML.load_file('banned_items.yml')

    def initialize(user)
      @user = user
    end

    def name
      @user.nick.nil? ? @user.username : @user.nick
    end

    def character?
      !character.nil?
    end

    def class
      return @class if defined? @class

      @class = Realmeye::Api::Character.for(user_name).character_class
    end

    def parse_slot?(slot)
      case slot
      when 'weapon'
        parse_weapon?
      when 'armor'
        parse_armor?
      when 'ability'
        parse_ability?
      when 'ring'
        parse_ring?
      end
    end

    def parse_weapon?
      !BANNED_EQUIPMENT.include?(character.weapon)
    end

    def parse_armor?
      !BANNED_EQUIPMENT.include?(character.armor)
    end

    def parse_ability?
      !BANNED_EQUIPMENT.include?(character.ability)
    end

    def parse_ring?
      !BANNED_EQUIPMENT.include?(character.ring)
    end

    def parse_attack
      character.to_max('attack')
    end

    def parse_dexterity
      character.to_max('dexterity')
    end

    private

    def character
      @character ||= Realmeye::Api::Character.for(user_name)
    end

    def user_name
      @user.nick.nil? ? @user.username.gsub(/[^\w\d]/, '') : @user.nick.gsub(/[^a-zA-Z0-9]/, '')
    end
  end
end
