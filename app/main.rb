# frozen_string_literal: true

require 'discordrb'
require_relative 'domain/commands/base.rb'
require_relative 'domain/receivers/base.rb'

require_relative 'domain/storage/afk.rb'
require_relative 'domain/storage/vetafk.rb'
require_relative 'domain/storage/logs.rb'

require_relative 'domain/discord/channel/base.rb'

class Main
  attr_accessor :token, :commands

  def initialize(token:)
    @token = token
  end

  def main!
    bot = Discordrb::Bot.new(token: @token)
    setup!(bot)

    bot.run unless ENV['BOT_ENV'] == 'testing'
  end

  private

  def setup!(bot)
    Receivers::Base.new.start_receivers(bot)
    load_helpers
    load_senders
  end

  def load_helpers
    Dir["#{File.dirname(__FILE__)}/domain/helpers/**/*.rb"].sort.each { |file| require File.absolute_path(file) }
  end

  def load_senders
    Dir["#{File.dirname(__FILE__)}/domain/senders/**/*.rb"].sort.each { |file| require File.absolute_path(file) }
  end
end
