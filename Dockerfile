FROM ruby:2.7.1

WORKDIR /usr/src/app

ADD Gemfile /usr/src/app
ADD Gemfile.lock /usr/src/app
RUN bundle install

CMD [ "bundle", "exec", "ruby", "run.rb" ]

ADD . /usr/src/app

