# frozen_string_literal: true

require_relative 'app/main.rb'
require_relative 'app/domain/config'

Main.new(token: Config.load('token')).main!
